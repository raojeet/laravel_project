<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'success' => 'Record saved successfully.',
    'invalid_password' => 'Please enter valid password.',
	'group_follow_success' => 'Group followed successfully.',
    'error' => 'Sorry, system is down at the moment.',
    'invalid_json' => 'Invalid json input.',
    'otp_sent' => 'We have sent you otp. Please use 1234 as default otp if not received.',
    'otp_verified' => 'Verified successfully.',
    'invalid_otp' => 'Invalid otp code.',
    'invalid_phone' => 'Please enter 10 digit mobile number.',
    'not_verified' => 'Hey, please sign up and verify and your mobile first',
    'event_added' => 'Event added successfully.',
    'event_update' => 'Event updated successfully.',
    'event_delete' => 'Event deleted successfully.',
    'post_added' => 'Post added successfully.',
    'post_update' => 'Post updated successfully.',
    'post_delete' => 'Post deleted successfully.',
    'post_description' => 'Post description required.',
    'email_verify' => 'Your email is not verified.',
    'email_not_verified' => 'Hey! Your email seems to be not verified. Please check your inbox.',
    'bike_added' => 'Bike added successfully.',
    'bike_image_added' => 'Bike images added successfully.',
    'user_delete' => 'User deleted from group successfully.',
    'signup_subject' => 'BikesNBuddies : Welcome to Bike n Buddies!',
    'reset_password_subject' => 'BikesNBuddies : Reset password email',
    'reset_pass_mail_sent' => 'A temporary password has been sent on your registered email.',
];
