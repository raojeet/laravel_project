<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Notification Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during Notification for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'user_follow_title' => 'Follow',
    'user_follow_desc' => 'Follow!',
    
    'group_follow_title' => 'Group Follow',
    'group_follow_desc' => 'Group Follow!',

    'group_join_title' => 'GroupJoin',
    'group_join_desc' => 'Group Join!',

    'group_event_title' => 'Eventcreate',
    'group_event_desc' => 'Event create!',

    'event_create_title' => 'Event Create ',
    'event_create_desc' => 'Event Create !',

    'event_expire_title' => 'Event Expire ',
    'event_expire_desc' => 'Event Expire !',
    
    'post_create_title' => 'Post Create ',
    'post_create_desc' => 'Post Create !',
    
    'post_like_title' => 'Post Like ',
    'post_like_desc' => 'Post Like !',
    
    'post_comment_title' => 'Post Comment ',
    'post_comment_desc' => 'Post Comment !',
];
