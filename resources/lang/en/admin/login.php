<?php

return [
    'text' => 'Login',
    'email' => 'email',
    'password' => 'password',
    'forget' => 'I have forgotten my password !',
    'maxattempt' => 'You have reached the maximum number of login attempts. Try again in one minute.',
    'app' => 'You are not authorized.',
    'credentials' => 'These credentials do not match our records.',
];
