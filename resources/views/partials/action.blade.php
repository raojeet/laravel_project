@if(isset($edit) && !empty($edit))
	@if(isset($group->is_admin_approved) && !empty($group->is_admin_approved))
		<label class="btn btn-success btn-xs openModal" style="background-color:#5cb85c;border-color:#5cb85c">Approved</a>
	@else
		<a class="btn btn-primary btn-xs openModal" data-id="{{$group->id}}" data-toggle="modal" data-target="#myModalActivation" >Approve</a>
	@endif
@endif