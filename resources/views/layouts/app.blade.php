<?php
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0 "); // Proxies.
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ trans('admin/site.title') }}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Bootstrap 3.3.6 -->
        {!! HTML::style('plugins/bootstrap/css/bootstrap.min.css') !!}
        {!! HTML::style('plugins/font-awesome/css/font-awesome.min.css') !!}
        <!-- Theme style -->
        {!! HTML::style('css/AdminLTE.min.css') !!}
        {!! HTML::style('css/skins/_all-skins.min.css') !!}
        {!! HTML::style('css/developer.css') !!}
        @yield('styles')
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="fixed sidebar-mini skin-red-light">
        <div class="wrapper">
            @include('layouts/partials.header')
            @include('layouts/partials.sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')
            </div>
            <!-- /.content-wrapper -->
            @include('layouts/partials.footer')

        </div>

        <!-- jQuery 2.2.3 -->
        {!! HTML::script('plugins/jQuery/jquery-2.2.3.min.js') !!}
        <!-- Bootstrap 3.3.6 -->
        {!! HTML::script('plugins/bootstrap/js/bootstrap.min.js') !!}
        <!-- AdminLTE App -->
        {!! HTML::script('js/main.js') !!}
        {!! HTML::script('plugins/slimScroll/jquery.slimscroll.min.js') !!}
        @stack('scripts')
        <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function() {
            $('#logout').click(function(e) {
                e.preventDefault();
                $('#logout-form').submit();
            });
        });
        window.onpageshow = function(event) {
            if (event.persisted) {
                window.location.reload() 
            }
        };
    </script>
    </body>
</html>
