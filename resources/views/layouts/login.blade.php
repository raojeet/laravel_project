<?php
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0 "); // Proxies.
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ trans('admin/site.title') }}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        {!! HTML::style('plugins/bootstrap/css/bootstrap.min.css') !!}
        {!! HTML::style('plugins/font-awesome/css/font-awesome.min.css') !!}
        <!-- Theme style -->
        {!! HTML::style('css/AdminLTE.min.css') !!}
        {!! HTML::style('css/developer.css')!!}
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition login-page" style="background: url({{url('/images/bg.jpg')}});">
        @yield('content')
        {!! HTML::script('plugins/jQuery/jquery-2.2.3.min.js') !!}
        <!-- Bootstrap 3.3.6 -->
        {!! HTML::script('plugins/bootstrap/js/bootstrap.min.js') !!}
        <script>
        window.onpageshow = function(event) {
            if (event.persisted) {
                window.location.reload() 
            }
        };
    </script>
    </body>
</html>
