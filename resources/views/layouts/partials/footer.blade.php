<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2017-18 <a href="#">{{trans('admin/site.app_name')}}</a>.</strong> All rights reserved.
</footer>