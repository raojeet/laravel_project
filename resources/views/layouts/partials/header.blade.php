<header class="main-header">
    <a href="{{url('/')}}" class="logo">
        <span class="logo-mini"><img src="{{url('images/logo_white.png')}}" width="32"> <b>L 2B</b></span>
        <span class="logo-lg text-left"><img src="{{url('images/logo_white.png')}}" width="32"> <b>{{trans('admin/site.app_name')}}</b></span>
    </a>
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
    </nav>
</header> 