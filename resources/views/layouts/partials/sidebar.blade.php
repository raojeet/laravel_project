<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                &nbsp;
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Admin</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="{{url('home')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview">
                <a href="{{url('users')}}">
                    <i class="fa fa-user"></i>
                    <span>{{trans('admin/site.users')}}</span>
                </a>
            </li>
            <li class="treeview">
                <a href="{{url('bikers')}}">
                    <i class="fa fa-user"></i>
                    <span>{{trans('admin/site.bikers')}}</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-question-circle" aria-hidden="true"></i>
                    <span>{{trans('admin/site.groups')}}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('groups')}}"><i class="fa fa-circle-o"></i> View</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="{{url('password/change')}}">
                    <i class="fa fa-gears"></i>
                    <span>Change Password</span>
                </a>
            </li>

            <li class="treeview">               
                <a href="{{url('logout')}}" id="logout">
                    <i class="fa fa-power-off"></i>
                    <span>Sign out</span>
                </a>
                {!! Form::open(['url' => '/logout', 'id' => 'logout-form']) !!}{!! Form::close() !!}
            </li>

        </ul>
    </section>
</aside>