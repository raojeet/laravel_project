@extends('layouts.app')
@section('styles')
{!! HTML::style('plugins/datatables/dataTables.bootstrap.css') !!}
{!! HTML::style('css/developer.css') !!}
@stop
@section('content')
@include('dashboard.partials.breadcum', [
'title' => trans('admin/site.bikers'),
'icon' => 'info-circle'
]) 
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <form class="form-horizontal" method="POST" enctype="multipart/form-data"
                  accesskey=""   accept-charset="UTF-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Biker Details</b>
                            <div style="float:right">
                                | <a style="position: relative;" href="{{ url('bikers') }}">Back</a></b>
                            </div>
                        </div>
                        <div class="panel-body">

                            <div class="form-group row">
                                <label class="control-label col-sm-4 leftalign">Logo :</label>
                                <img class="leftalign" style="height:150px; width:200px; margin-left:15px; " src="{{url('images/users/'.$profile->image)}}"/>
                            </div>

                            <div class="form-group row">
                                <label class="control-label col-sm-4 leftalign">Name : </label>
                                <label class="control-label col-sm-6 leftalign">{{$profile->name}}</label>
                            </div>

                            <div class="form-group row">
                                <label class="control-label col-sm-4 leftalign">Location :</label>
                                <label class="control-label col-sm-6 leftalign">{{$profile->location}}</label>
                            </div>

                            <div class="form-group row">
                                <label for="weekdayOpeningTime" class="control-label col-sm-4 leftalign">Gender :</label>
                                @if($profile->gender=='M')
                                    <label class="control-label col-sm-6 leftalign">Male</label>
                                @else 
                                    <label class="control-label col-sm-6 leftalign">Female</label>
                                @endif
                            </div>

                            <div class="form-group row">
                                <label class="control-label col-sm-4 leftalign">Email :</label>
                                <label class="control-label col-sm-6 leftalign">{{ $profile->email }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>Bikes</b>
                    </div>
                    <div class="panel-body">
                        <div class="row" style="padding: 10px;">
                            @if (isset($profile))

                            @foreach ($profile->bike as $bike)

                                <div class="form-group row">
                                    <label class="control-label col-sm-4 leftalign">Bike Company : </label>
                                    <label class="control-label col-sm-6 leftalign">{{$bike->bike_company}}</label>
                                </div>

                                <div class="form-group row">
                                    <label class="control-label col-sm-4 leftalign">Bike Model :</label>
                                    <label class="control-label col-sm-6 leftalign">{{$bike->bike_modal}}</label>
                                </div>

                                <div class="form-group row">
                                    <label for="weekdayOpeningTime" class="control-label col-sm-4 leftalign">Bike Number :</label>
                                    <label class="control-label col-sm-6 leftalign">{{ $bike->bike_number }}</label>
                                </div>

                                @foreach ($bike->bikeImages as $bikeImage)
                                <div class="additionalImageDiv">
                                    <img src="{{$bikeImage->full_url}}" style="width:180px;height:150px" class="img-thumbnail" >
                                </div>
                                @endforeach  
                                <div style="clear:both"></div>
                                <hr/>
                            @endforeach 

                            @endif

                        </div>
                    </div>
                </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection