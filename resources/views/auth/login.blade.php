@extends('layouts.login')
@section('content')
<div class="login-box"> 
    <div class="login-logo">
        <a href="{{url('/')}}" style="color: #fff;"><b>{{trans('admin/site.title')}}</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="panel panel-default">
        <div class="panel-heading bg-color">{{ trans('admin/login.text') }}</div>
        <div class="panel-body">

            @if(session()->has('error'))
            @include('partials/error', ['type' => 'danger', 'message' => session('error')])
            </hr>
            @endif

            {!! Form::open(['url' => 'login', 'method' => 'post', 'role' => 'form']) !!}
            {!! Form::controlBootstrap('email', 0, 'email', $errors, trans('admin/password.email'),null,null,trans('admin/site.email'),'envelope') !!}
            {!! Form::controlBootstrap('password', 0, 'password', $errors, trans('admin/password.password'),null,null,trans('admin/password.password'),'lock') !!}
            <div class="row">
                <!-- /.col -->
                <div class="col-xs-4 pull-right">
                    <button type="submit" id="loginbtn" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
            {!! Form::close() !!}
            {!! link_to('password/reset', trans('admin/login.forget')) !!}
        </div>
    </div>
</div>

@endsection