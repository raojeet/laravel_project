@extends('layouts.app')
@section('content')

@include('dashboard.partials.breadcum', [
'title' => trans('admin/site.change_password'),
'icon' => 'gears'
])
<section class="content">
    <!-- /.content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Change Password</div>
                    <div class="panel-body">
                        @if(session()->has('success'))
                            @include('partials/error', ['type' => 'success', 'message' => session('success')])
                        @endif
                        @if(session()->has('error'))
                            @include('partials/error', ['type' => 'danger', 'message' => session('error')])
                        @endif

                        {!! Form::open(['url' => 'password/change', 'method' => 'post', 'role' => 'form']) !!}
                        {!! Form::controlBootstrap('password', 0, 'old_password', $errors, trans('admin/password.old_password'),null,null,trans('admin/password.old_password'),'lock') !!}
                        {!! Form::controlBootstrap('password', 0, 'password', $errors, trans('admin/password.new_password'),null,null,trans('admin/password.new_password'),'lock') !!}
                        {!! Form::controlBootstrap('password', 0, 'password_confirmation', $errors, trans('admin/site.confirm_password'),null,null,trans('admin/site.confirm_password'),'lock') !!}
                        {!! Form::submitBootstrap(trans('admin/site.submit')) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection