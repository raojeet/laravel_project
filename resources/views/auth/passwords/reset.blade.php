@extends('layouts.login')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="login-box">
            <div class="login-logo">
                <a href="{{url('/')}}" style="color: #ffffff;"><b>{{trans('admin/site.title')}}</b></a>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading bg-color">Reset Password</div>
                <div class="panel-body">
                    @if(session()->has('error'))
                    @include('partials/error', ['type' => 'danger', 'message' => session('error')])
                    @endif
                    {!! Form::open(['url' => 'password/reset', 'method' => 'post', 'role' => 'form']) !!}
                    {!! Form::hidden('token', $token) !!}
                    {!! Form::controlBootstrap('email', 12, 'email', $errors, trans('admin/password.email'),null,null,trans('admin/site.email'),'envelope') !!}
                    {!! Form::controlBootstrap('password', 12, 'password', $errors, trans('admin/password.password'),null,null,trans('admin/site.password'),'lock') !!}
                    {!! Form::controlBootstrap('password', 12, 'password_confirmation', $errors, trans('admin/site.confirm_password'),null,null,trans('admin/site.confirm_password'),'lock') !!}
                    {!! Form::submitBootstrap(trans('admin/site.submit'), 'col-lg-12') !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection