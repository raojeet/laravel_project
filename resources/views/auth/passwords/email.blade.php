@extends('layouts.login')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="login-box">
            <div class="login-logo">
                <a href="{{url('/')}}" style="color: #ffffff;"><b>{{trans('admin/site.title')}}</b></a>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading bg-color">Forgot Password</div>
                <div class="panel-body">
                    @if(session()->has('status'))
                    @include('partials/error', ['type' => 'success', 'message' => session('status')])
                    @endif
                    @if(session()->has('error'))
                    @include('partials/error', ['type' => 'danger', 'message' => session('error')])
                    @endif

                    {!! Form::open(['url' => 'password/email', 'method' => 'post', 'role' => 'form']) !!} 
                    {!! Form::controlBootstrap('email', 0, 'email', $errors, trans('admin/password.email'),null,null,trans('admin/site.email'),'envelope') !!}
                    {!! Form::submitBootstrap(trans('admin/site.send'), 'col-lg-12') !!}

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection