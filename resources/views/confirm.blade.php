<!doctype html>
<html>
    <head>
        <title>Verify</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
       <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
        <style>
            html, body {
                height: 100%;
            }
            body {
                font-family: 'Roboto', sans-serif;
                margin: 0;
                padding: 0;
                background: #02B3DB no-repeat center center;
                background-size: cover;
            }
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
            }
            .wrapper {
                width: 100%;
                display: table;
                height: 100%;
            }
            .page-inner {
                display: table-cell;
                vertical-align: middle;
            }
            .logo {
                width: 100%;
                text-align: center;
                margin-top: 30px;
            }
            .content {
                width: 100%;
                padding: 0 20px;
                text-align: center;
                margin-top: 50px;
            }
            .content h1 {
                font-size: 46px;
                color: #fff;
            }
            .content p {
                font-size: 24px;
                font-weight: bold;
                color: #fff;
                line-height: 40px;
            }
            /*.already-verify {
                display: none;
            }*/
        </style>

    </head>

    <body>
        <div class="wrapper">
            <div class="page-inner">
                <div class="content">
                    @if($result_verify)
                    <div class="verify">
                        <h1>Congratulations!</h1>
                        <p>You have successfully verified your email.</p>
                    </div>
                    @else
                    <div class="already-verify">
                        <p>You have already accepted the Invitation. <br>Please login to Bikes n Buddies and start creating your memories.</p>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        
        
    </body>
    </html>