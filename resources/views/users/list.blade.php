@extends('layouts.app')
@section('styles')
{!! HTML::style('plugins/datatables/dataTables.bootstrap.css') !!}
{!! HTML::style('css/developer.css') !!}
@stop
@section('content')
@include('dashboard.partials.breadcum', [
'title' => trans('admin/site.users'),
'icon' => 'info-circle'
])
<section class = "content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">&nbsp;</div><!-- /.box-header -->
                <div class="box-body">
                    <div class="alert alert-success hide" role="alert">
                        {!! trans('admin/site.group_approved') !!}
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" class="form-control input-sm col-xs-4" placeholder="Search keyword..." name="userName" value="" id="userName">
                                <div class="input-group-btn">
                                <button class="btn btn-sm btn-default" id="btn-search" type="button"><i class="fa fa-search"></i></button>
                            </div>
                            </div>
                        </div>
                    </div>
                    <table id="group-table" class="table table-striped table-bordered nowrap dataTable no-footer">
                        <thead>
                            <tr>
                                <!-- <th>Id</th> -->
                                <th>Name</th>
                                <th>Email</th>
                                <th>City</th>
                                <th>Country</th>
                            </tr>
                        </thead>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
@include('dashboard.partials.activemodal', [
'title' => trans('admin/site.approve-alert'),
'link' => 'group/approve/'
])
@push('scripts')
{!! HTML::script('plugins/datatables/jquery.dataTables.min.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.min.js') !!}
<script>
    $(function () {
        var oTable = $('#group-table').DataTable({
            processing: true,
            serverSide: true,
            paging: true,
            bFilter: false,
            pageLength: 15,
            bLengthChange: false,
            autoWidth: false,
            binfo: true,
            info: true,
            scrollX: true,
            order: [[0, "asc"]],
            columnDefs: [
                { width: '60%', targets: 1 }
            ],
            ajax: {
                url: '{!! route('datatables.allUserData') !!}',
                data: function (d) {
                    console.log(d);
                    d.userName = $('#userName').val();
                },
                error: function (xhr, status, error) {
                    console.log(error);
                },
            },
            columns: [
                // {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'city', name: 'city'},
                {data: 'country', name: 'country', orderable: false, searchable: false}
            ],
            error: function (xhr, status, error) {
                console.log(error);
            },
        });
        $("#btn-search").click(function (e) {
            oTable.draw();
            e.preventDefault();
        })
    });
    $(document).on("click", ".openModal", function () {
        var userId = $(this).data('id');
        $(".modal-footer #UnhideUserId").val( userId );
    });
    function formSubmit() {

        $('#myModalActivation').modal('hide');
        var url = $('#url').val()+$('#UnhideUserId').val();
        $.post(url,function(data){
            $('.alert-success').removeClass('hide');
        });
    }
</script>

@endpush

@endsection

