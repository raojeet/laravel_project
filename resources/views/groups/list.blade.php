@extends('layouts.app')
@section('styles')
{!! HTML::style('plugins/datatables/dataTables.bootstrap.css') !!}
{!! HTML::style('css/developer.css') !!}
@stop
@section('content')
@include('dashboard.partials.breadcum', [
'title' => trans('admin/site.groups'),
'icon' => 'info-circle'
])
<section class = "content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">&nbsp;</div><!-- /.box-header -->
                <div class="box-body">
                    <div class="alert alert-success hide" role="alert">
                        {!! trans('admin/site.group_approved') !!}
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                                <input type="text" class="form-control input-sm col-xs-4" placeholder="Search keyword..." name="userName" value="" id="userName">
                                <div class="input-group-btn">
                                <button class="btn btn-sm btn-default" id="btn-search" type="button"><i class="fa fa-search"></i></button>
                            </div>
                            </div>
                        </div>
                    </div>
                    <table id="group-table" class="table table-striped table-bordered nowrap dataTable no-footer">
                        <thead>
                            <tr>
                                <th>Group</th>
                                <th>Group Owner</th>
                                <th>Tag Line</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Created Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
</section>
@include('dashboard.partials.activemodal', [
'title' => trans('admin/site.approve-alert'),
'link' => 'group/approve/'
])
@push('scripts')
{!! HTML::script('plugins/datatables/jquery.dataTables.min.js') !!}
{!! HTML::script('plugins/datatables/dataTables.bootstrap.min.js') !!}
<script>
    $(function () {
        var oTable = $('#group-table').DataTable({
            processing: true,
            serverSide: true,
            paging: true,
            bFilter: false,
            pageLength: 15,
            bLengthChange: false,
            autoWidth: false,
            binfo: true,
            info: true,
            scrollX: true,
            order: [[0, "asc"]],
            columnDefs: [
                { width: '60%', targets: 1 }
            ],
            ajax: {
                url: '{!! route('datatables.allGroupData') !!}',
                data: function (d) {
                    d.userName = $('#userName').val();
                },
                error: function (xhr, status, error) {
                    console.log(error);
                },
            },
            columns: [
                {data: 'title', name: 'title'},
                {data: 'name', name: 'name'},
                {data: 'tag_line', name: 'tag_line'},
                {data: 'city', name: 'city'},
                {data: 'state', name: 'state'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ],
            error: function (xhr, status, error) {
                console.log(error);
            },
        });
        $("#btn-search").click(function (e) {
            oTable.draw();
            e.preventDefault();
        })
    });
    $(document).on("click", ".openModal", function () {
        var groupId = $(this).data('id');
        $(".modal-footer #UnhideUserId").val( groupId );
    });
    function formSubmit() {

        $('#myModalActivation').modal('hide');
        var url = $('#url').val()+$('#UnhideUserId').val();
        $.post(url,function(data){
            $('.alert-success').removeClass('hide');
            window.location.reload();
        });
    }
</script>

@endpush

@endsection

