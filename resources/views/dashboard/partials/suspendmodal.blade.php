<div id="myModalDeactivation" class="modal fade in" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onClick="delText()" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirm</h4>
            </div>
            <div class="modal-body">
                <form class="myForm" name="hideReasonForm" id="hideReasonForm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="hide_url" value="{{ $link }}">
                    <p> {!! $title !!} </p>
                    <input type="text" class="form-control" id="hideReason" name="hideReason" placeholder="Reason to Suspend">
                    <label id="modalLabel" class="myLabel" style="color: red;"></label><br>
                    <input type="hidden" value="" name="hideId" id="hideId">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onClick="delText()" data-dismiss="modal">Cancel</button>
                <input type="submit" class="btn btn-success" id="hideSubmit" value="Submit">
            </div>
        </div>
    </div>
</div>