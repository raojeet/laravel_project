<section class="content-header">
    <h4>
        <i class="fa fa-{{ $icon }}"></i> {!! $title !!}
    </h4>
</section>