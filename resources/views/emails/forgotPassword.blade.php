<!doctype html>
<html lang="en">
    <head>
        <title>Bikes n Buddies</title>
    </head>
    <body>

        <table border="0" width="600" align="center" cellspacing="0" cellpadding="0" style="border: 1px solid rgb(229,236,242); font-family: arial;">
            <tr>
                <td background="">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" style="padding: 10px 0 0px;"><img src="" alt="Bikes n Buddies" style="width: 60px;"></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-size: 20px; color: #ffffff; padding: 0 0 5px;">Welcome to Bikes n Buddies!</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding: 40px 30px;">
                    <h1 style="margin: 0 0 25px; color: rgb(44,46,48); font-size: 18px; line-height: 20px;">Hello,</h1>
                    <p style="margin: 0; color: rgb(44,46,48); font-size: 14px; line-height: 20px;">You are receiving this email because we received a password reset request for your account. 
                        Click the button below to reset your password: 
                    </p>
                    {{-- Action Button --}}
                    @if (isset($actionText))
                    <?php
                    switch ($level) {
                        case 'success':
                            $color = 'green';
                            break;
                        case 'error':
                            $color = 'red';
                            break;
                        default:
                            $color = 'blue';
                    }
                    ?>
                    @component('mail::button', ['url' => $actionUrl, 'color' => $color])
                    {{ $actionText }}
                    @endcomponent
                    @endif
                    <p style="margin: 0; color: rgb(44,46,48); font-size: 14px; line-height: 20px;">If you did not request a password reset, no further action is required.</p>
                    <p style="margin: 30px 0 0; color: rgb(44,46,48); font-size: 14px; line-height: 20px;"><strong>Warm Regards,</strong><br>Bikes n Buddies Team</p>
                </td>
            </tr>
            <tr>
                <td bgcolor="#02B3DB">
                    <p style="margin: 20px 0; color: #fff; text-align: center; font-size: 14px; line-height: 20px;">&copy; 2017 Bikes n Buddies. All rights reserved.</p>
                </td>
            </tr>
        </table>
    </body>
</html>