<!doctype html>
<html lang="en">
    <head>
        <title>Bikes n Buddies</title>
    </head>
    <body>
        <table border="0" width="600" align="center" cellspacing="0" cellpadding="0" style="border: 1px solid rgb(229,236,242); font-family: arial;">
            <tr>
                <td background="#02B3DB">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center" style="font-size: 20px; color: #ffffff; padding: 0 0 5px;">Welcome to Bikes n Buddies!</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding: 40px 30px;">
                    <h1 style="margin: 0 0 25px; color: rgb(44,46,48); font-size: 13px; line-height: 20px;">Hello,</h1>
                    <p style="margin: 0; color: rgb(44,46,48); font-size: 12px; line-height: 20px;">
                        Thank you for registering to Bikes n Buddies.
                        <br><br>Please click here to verify your email. <a href="{{ url($data['url']) }}" style="color: rgb(38,220,99);">Click Here</a>
                        <br><br>OR Copy and paste below mentioned link on your browser to verify your email address.<br>
                        {{ url($data['url']) }}<br><br>
                    </p>
                    <p style="margin: 0; color: rgb(44,46,48); font-size: 12px; line-height: 20px;">Thank you for registering to Bikes n Buddies.</p>
                    <p style="margin: 30px 0 0; color: rgb(44,46,48); font-size: 13px; line-height: 20px;">Warm Regards,<br>Bikes n Buddies Team</p>
                </td>
            </tr>
            <tr>
                <td bgcolor="#02B3DB">
                    <p style="margin: 20px 0; color: #fff; text-align: center; font-size: 14px; line-height: 20px;">&copy; 2017 Bikes n Buddies. All rights reserved.</p>
                </td>
            </tr>
        </table>
    </body>
</html>