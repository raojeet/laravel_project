<!doctype html>
<html lang="en">
    <head>
        <title>BikesNBuddies</title>
    </head>
    <body>

        <table border="0" width="600" align="center" cellspacing="0" cellpadding="0" style="border: 1px solid rgb(229,236,242); font-family: arial;">
            <tr>
                <td style="padding: 40px 30px;">
                    <h1 style="margin: 0 0 25px; color: rgb(44,46,48); font-size: 18px; line-height: 20px;">Hello {{ $data['user_name'] }},</h1>
                    <p style="margin: 0; color: rgb(44,46,48); font-size: 14px; line-height: 20px;">Your password has been reset successfully.<br><br> 
                        Please use <strong>"{{ $data['password'] }}"</strong> to login into App account. We strongly recommend that you change your password to something that you will remember once you have logged back in.</p>
                    <p style="margin: 30px 0 0; color: rgb(44,46,48); font-size: 14px; line-height: 20px;"><strong>Warm Regards,</strong><br>BikesNBuddies Team</p>
                </td>
            </tr>
            <tr>
                <td bgcolor="#02B3DB">
                    <p style="margin: 20px 0; color: #fff; text-align: center; font-size: 14px; line-height: 20px;">&copy; 2017 BikesNBuddies. All rights reserved.</p>
                </td>
            </tr>
        </table>
    </body>
</html>