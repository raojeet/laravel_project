<div class="form-group {{ $class }}">
   {!! Form::submit($value, ['class' => 'btn btn btn-primary']) !!}
</div>