swagger: '2.0'
consumes:
   - application/json
produces:
   - application/json
   
info:
  title: BikesNBuddies API
  description: |
    Complete Documentation of BikesNBuddies API
    ####  API ENDPOINT : http://<?php echo $_SERVER['SERVER_NAME']; ?><?php if(preg_match("/\blocalhost\b/i", $_SERVER['SERVER_NAME'], $match)) { echo explode("/public", dirname($_SERVER['REQUEST_URI']))[0].'/public';  } ?>/api/v1
  version: "1.0.0"
  
definitions: 
   Error:
    type: object
    properties:
      message:
        type: string
      result:
        type: object
        description: Key/value result/error
    
   SignupObject:
    type: object
    properties:
      email: 
        type: string
        example: ajay.sherawat@appster.in
      password:
        type: string
        example: 12345678
        
   SigninObject:
    type: object
    properties:
      email: 
        type: string
        example: ajay.sherawat@appster.in
      password:
        type: string
        example: 12345678
      deviceType:
        type: string
        example: ANDROID
      deviceToken:
        type: string
        example: AQDX124324
        
   ForgotPasswordObject:
    type: object
    properties:
      email: 
        type: string
        example: ajay.sherawat@appster.in
        
   ChangePasswordObject:
    type: object
    properties:
      oldPassword: 
        type: string
        example: 123456
      password: 
        type: string
        example: 1234567
        
   ProfileObject:
    type: object
    properties:
      name: 
        type: string
        example: Amit
      dob: 
        type: string
        format: date
        example: '1990-12-23' 
      userName: 
        type: string
        example: amit12
    
   ValidateUserNameObject:
    type: object
    properties:
      userName: 
        type: string
        example: amit12
        
   ValidateUpdateTokenObject:
    type: object
    properties:
      deviceToken: 
        type: string
        example: amit12
        
   MomentObject:
    type: object
    properties:
      page: 
        type: integer
        example: 0
      syncDateTime: 
        type: string
        format: date
        example: '2017-12-12 10:10:12'
      status: 
        type: integer
        example: 0
      
        
   NotificationObject:
    type: object
    properties:
      page: 
        type: integer
        example: 0
        
   DeleteMomentObject:
    type: object
    properties:
      momentServerId: 
        type: integer
        example: 0
   
   iOSTransactionObject:
    type: object
    properties:
      productId: 
        type: string
        example: com.swingbyswing.swingu.quarterlysubscription
      base64Data: 
        type: string
        example: optional
        
   AndroidTransactionObject:
    type: object
    properties:
      productId: 
        type: string
        example: com.swingbyswing.swingu.quarterlysubscription
      purchaseToken: 
        type: string
        format: date
        example: Tx86798GH88
      packageName: 
        type: string
        example: required
      orderId: 
        type: string
        example: optional
        
   settingObject:
    type: object
    properties:
      wifi: 
        type: integer
        example: 0
   
        
schemes:
  - http
host: <?php echo $_SERVER['SERVER_NAME']; ?>

basePath: <?php if(preg_match("/\blocalhost\b/i", $_SERVER['SERVER_NAME'], $match)) { echo explode("/public", dirname($_SERVER['REQUEST_URI']))[0].'/public';  } ?>/api/v1
paths:
  /signup:
    post:
      summary:
        - User sign up
      consumes:
        - application/json
      produces:
        - application/json
      description: Used to sign up user with email and password
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         schema:
           $ref: '#/definitions/SignupObject'
           
  /signin:
    post:
      summary:
        - User login
      consumes:
        - application/json
      produces:
        - application/json
      description: Used to sign in user with email and password
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: deviceType must be in ANDROID or IPHONE
         schema:
           $ref: '#/definitions/SigninObject'
  
  /forgotPassword:
    post:
      summary:
        - Forgot /reset password
      consumes:
        - application/json
      produces:
        - application/json
      description: Send temporary password in mail.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/ForgotPasswordObject'
           
  /changePassword:
    post:
      summary:
        - Change password
      consumes:
        - application/json
      produces:
        - application/json
      description: Change Password.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/ChangePasswordObject'
       - name: sessionId
         in: header
         required: true
         description: current session
    
  /saveProfile:
    post:
      summary:
        - Update user profile
      consumes:
        - application/json
      produces:
        - application/json
      description: Save & update user profile.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/ProfileObject'
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /saveImage:
    post:
      summary:
        - Update profile picture
      consumes:
        - application/json
      produces:
        - application/json
      description: Save user profile image.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: sessionId
         in: header
         required: true
         description: current session
       - name: image
         in: formData
         description: valid image file
         type: file
         
  /logout:
    post:
      summary:
        - Logout user from the app
      consumes:
        - application/json
      produces:
        - application/json
      description: User logout.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /resendEmail:
    post:
      summary:
        - Send verification link
      consumes:
        - application/json
      produces:
        - application/json
      description: Verify BikesNBuddies account email.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/ForgotPasswordObject'
           
  /validateUserName:
    post:
      summary:
        - Validate unique user name
      consumes:
        - application/json
      produces:
        - application/json
      description: validate user name is available or not.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/ValidateUserNameObject'
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /profile:
    get:
      summary:
        - Get current user profile
      consumes:
        - application/json
      produces:
        - application/json
      description: Get user profile.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /updateToken:
    post:
      summary:
        - Update user device Token
      consumes:
        - application/json
      produces:
        - application/json
      description: Update user device Token.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/ValidateUpdateTokenObject'
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /questions:
    get:
      summary:
        - Get questions according to user age group
      consumes:
        - application/json
      produces:
        - application/json
      description: Get questions according to user age group.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /moments:
    post:
      summary:
        - Get moments of logged in user
      consumes:
        - application/json
      produces:
        - application/json
      description: active moments with status 0 & for quarantine use status 2 .
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/MomentObject'
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /notifications:
    post:
      summary:
        - Get Notifications of logged in user
      consumes:
        - application/json
      produces:
        - application/json
      description: Get Notifications of logged in user.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/NotificationObject'
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /deleteMoment:
    post:
      summary:
        - Delete moment by id
      consumes:
        - application/json
      produces:
        - application/json
      description: Delete moment by id.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/DeleteMomentObject'
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /usages:
    get:
      summary:
        - Get user status and subscription details
      consumes:
        - application/json
      produces:
        - application/json
      description: Get user status and subscription details.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /packages:
    get:
      summary:
        - Get subscriptions 
      consumes:
        - application/json
      produces:
        - application/json
      description: Get subscription package list.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /saveIosTransaction:
    post:
      summary:
        - Save iOS transaction
      consumes:
        - application/json
      produces:
        - application/json
      description: Save iOS transaction.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/iOSTransactionObject'
       - name: sessionId
         in: header
         required: true
         description: current session
  
  /saveAndroidTransaction:
    post:
      summary:
        - Save Android transaction
      consumes:
        - application/json
      produces:
        - application/json
      description: Save Android transaction.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/AndroidTransactionObject'
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /cancelSubscription:
    get:
      summary:
        - Cancel transaction
      consumes:
        - application/json
      produces:
        - application/json
      description: Cancel transaction.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: sessionId
         in: header
         required: true
         description: current session
         
  /settings:
    post:
      summary:
        - change settings
      consumes:
        - application/json
      produces:
        - application/json
      description: Change settings.
      responses:
        200:
          description: Success response
        400:
          description: Validation response
        default:
          description: Result
          schema:
             $ref: '#/definitions/Error'
      parameters:
       - name: body
         in: body
         required: true
         description: 
         schema:
           $ref: '#/definitions/settingObject'
       - name: sessionId
         in: header
         required: true
         description: current session
         
       
           
      
   
        