<?php

return [
    'pannels' => [
        [
            'color' => 'bg-yellow',
            'icon' => 'user',
            'model' => \App\Models\User::class,
            'name' => 'admin/site.users',
            'url' => 'users',
            'role' => \App\Models\User::USER_ROLE
        ],
        [
            'color' => 'bg-aqua',
            'icon' => 'calendar',
            'model' => \App\Models\User::class,
            'name' => 'admin/site.bikers',
            'url' => 'bikers',
            'role' => \App\Models\User::USER_BIKE_OWNER_ROLE
        ],
        [
            'color' => 'bg-aqua',
            'icon' => 'calendar',
            'model' => \App\Models\Group::class,
            'name' => 'admin/site.groups',
            'url' => 'groups',
            'role' => ''
        ]
    ],
];
