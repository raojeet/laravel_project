<?php

/*  array of Constants   */
return [

    'thumb_width' => env('THUMB_WIDTH',320),
    'thumb_height' => env('THUMB_HEIGHT',240),
    'APP_NAME' =>'BikesNBuddies',
];
