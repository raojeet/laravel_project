<?php

namespace App\Services;

class Status {
    
    const STATUS = 'status';
    
    /**
     * Set the login user statut
     *
     * @param  Illuminate\Auth\Events\Login $login
     * @return void
     */
    public static function setLoginStatus($login) {
        session([Status::STATUS => $login->user->getStatus()]);
    }

    /**
     * Set the visitor user statut
     *
     * @return void
     */
    public static function setVisitorStatus() {
        session([Status::STATUS => 'visitor']);
    }

    /**
     * Set the statut
     *
     * @return void
     */
    public static function setStatus() {
        if (!session()->has(Status::STATUS)) {
            session([Status::STATUS => auth()->check() ? auth()->user()->getStatus() : 'visitor']);
        }
    }

}
