<?php

namespace App\Services;

class PannelAdmin {

    /**
     * @var array
     */
    protected $infos;

    /**
     * Pannel color
     * @var string
     */
    public $color;

    /**
     * Pannel icon
     * @var string
     */
    public $icon;

    /**
     * Pannel model
     * @var string
     */
    public $model;

    /**
     * Pannel number
     * @var string
     */
    public $nbr;

    /**
     * Pannel name
     * @var string
     */
    public $name;

    /**
     * Pannel url
     * @var string
     */
    public $url;

    /**
     * Create a new Admin instance.
     *
     * @param  Array $infos
     * @return \App\Services\PannelAdmin
     */
    public function __construct(Array $infos) {
        $this->color = $infos['color'];
        $this->icon = $infos['icon'];
        $this->model = new $infos['model'];
        $this->name = $infos['name'];
        $this->url = $infos['url'];
        $this->role = $infos['role'];
        $model = ($infos['model'] == 'App\Models\User') ? false : true;
        return $this->compute($model, $this->role);
    }

    /**
     * Compute the pannel
     *
     * @return \App\Services\PannelAdmin
     */
    protected function compute($model = false, $role = false) {
        $this->name = trans($this->name);
        if($model) {
            $this->nbr = (object) ([
                'total' => $this->model->count()
            ]);
        } else {
            $this->nbr = (object) ([
                'total' => $this->model->where('role',$role)->where('name', '<>', null)->count()
            ]);
        }

        return $this;
    }

}
