<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bike extends Model {

    //constants
    const BIKE_FULL_IMG = '/images/bike/';
    const BIKE_THUMB_IMG = '/images/bike/thumb/';

    /**
     * The attributes that are mass assignable except id.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at',
    ];
    
    protected $primaryKey = 'id';
    
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function bikeImage() {
        return $this->hasMany(BikeImage::class);
    }
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public static function saveUpdateBike($request) {
        $model = new Bike();
        $model->bike_company = $request->bikeCompany;
        $model->bike_modal = $request->bikeModal;
        $model->bike_number = $request->bikeNumber;
        $model->registration_expiry_date = $request->registrationExpiryDate;
        $model->user_id = $request->user->user_id;
        $model->save();
    }
}
