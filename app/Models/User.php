<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable {

    use Notifiable;

    // constants

    const USER_FULL_IMG = '/images/user/';
    const USER_THUMB_IMG = '/images/user/thumb/';

    const USER_ROLE = 0;
    const USER_BIKE_OWNER_ROLE = 2;
    const USER_ADMIN_ROLE = 1;

    /**
     * The attributes that are mass assignable except id.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'created_at', 'updated_at',
    ];
    
    protected $primaryKey = 'id';

    /** function used to set has one eloquent relation
     * @param none
     * @return realtion
     */
    public function session() {
        return $this->hasMany(UserSession::class);
    }
    
    /** function used to set has one eloquent relation
     * @param none
     * @return realtion
     */
    public function event() {
        return $this->hasMany(Event::class);
    }
    
    /** function used to set has one eloquent relation
     * @param none
     * @return realtion
     */
    public function group() {
        return $this->hasMany(Group::class);
    }
    
    /** function used to set has one eloquent relation
     * @param none
     * @return realtion
     */
    public function post() {
        return $this->hasMany(Post::class);
    }
    
    /** function used to set has one eloquent relation
     * @param none
     * @return realtion
     */
    public function bike() {
        return $this->hasMany(Bike::class);
    }
    
    /** function used to set has one eloquent relation
     * @param none
     * @return realtion
     */
    public function postComment() {
        return $this->hasMany(PostComment::class);
    }
    
    /** function used to set has one eloquent relation
     * @param none
     * @return realtion
     */
    public function follow() {
        return $this->hasMany(UserFollow::class);
    }
    
    public static function getContact($request) {
        $users =  User::leftJoin('user_follows', 'users.id', '=', 'user_follows.following_id')
                ->select('users.*')->whereIn('phone',$request->contactList)
                ->where('users.id', '<>', $request->user->user_id)
                ->whereNull('following_id')->get();
        return ['users' => $users];
    }

    /**
     * get user list Version V1
     * @param  $inputs
     * @return object
     */
    public static function userList($inputs) {
        $search = $inputs->userName;
        $query = User::select('name', 'username', 'email', 'is_verify', 'id','created_at','city','country')
                //->where('is_verify', true)
                ->where('name', '<>', null)
                ->where('role', false) ;
        if (!empty($search)) {
            $query->where(function($query) use ($search) {
                $query->where('name', 'LIKE', "%$search%")
                        ->orWhere('email', 'LIKE', "%$search%")
                        ->orWhere('username', 'LIKE', "%$search%");
            });
        }
        return $query->get();
    }

    /**
     * get user list Version V1
     * @param  $inputs
     * @return object
     */
    public static function bikerList($inputs) {
        $search = $inputs->userName;
        $query = User::select('name', 'username', 'email', 'is_verify', 'id','created_at','city','country')
                //->where('is_verify', true)
                ->where('name', '<>', null)
                ->where('role', 2);
        if (!empty($search)) {
            $query->where(function($query) use ($search) {
                $query->where('name', 'LIKE', "%$search%")
                        ->orWhere('email', 'LIKE', "%$search%")
                        ->orWhere('username', 'LIKE', "%$search%");
            });
        }
        return $query->get();
    }
    
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token) {
        $this->notify(new ResetPasswordNotification($token));
    }
    
}
