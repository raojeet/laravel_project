<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BikeImage extends Model {

    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bike_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $timestamps = false;
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function bike() {
        return $this->belongsTo(Bike::class);
    }
}
