<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    //constants
    const POST_FULL_IMG = '/images/post/';
    const POST_THUMB_IMG = '/images/post/thumb/';
    const ALL = 'all';
    const GROUP = 'group';
    const FOLLOWING = 'following';

    /**
     * The attributes that are mass assignable except id.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'updated_at',
    ];
    
    protected $primaryKey = 'id';
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function postLike() {
        return $this->hasMany(PostLike::class);
    }
    

    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function postComment() {
        return $this->hasMany(PostComment::class);
    }
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function postImage() {
        return $this->hasMany(PostImage::class);
    }
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function group() {
        return $this->belongsTo(Group::class);
    }
    
    /**
     * Update an event image.
     *
     * @param int
     * @param array
     */
    public static function updateImage($post_id, array $post_data) {
         // Post::find($post_id)->update($post_data);
          $image = new PostImage();
          $image->post_id = $post_id;
          $image->file_name = $post_data['file_name'];
          $image->thumb_url = $post_data['thumb'];
          $image->full_url = $post_data['image'];
          $image->save();
    }

     /**
     * Update an event image.
     *
     * @param int
     * @param array
     */
    public static function updateVideo($post_id, array $post_data) {
          $image = new PostImage();
          $image->post_id = $post_id;
          $image->file_name = $post_data['file_name'];
          $image->full_url = $post_data['full_url'];
          $image->save();
          return ['videoData' => $image];
    }

      /**
     * Update an event image.
     *
     * @param int
     * @param array
     */
    public static function updateVideoThumb($refrence_id, array $post_data) {
        PostImage::find($refrence_id)->update($post_data); 
    }
}
