<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostLike extends Model {

    const LIKE = 1;
    const UNLIKE = 2;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $hidden = [ 'updated_at'];
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function post() {
        return $this->belongsTo(Post::class);
    }
    
    /**
     * function 
     * @param type $request
     */
    public static function like($request) {
        if($request->status == PostLike::UNLIKE) {
            $res = PostLike::whereUserId($request->user->user_id)->wherePostId($request->postId)->first();
            if($res){
                PostLike::whereUserId($request->user->user_id)->wherePostId($request->postId)->delete();
                $post = Post::whereId($request->postId)->first();
                $count = $post->like_count - 1; 
                $post->update(['like_count' => $count]);
            }
        } else {
            $model = new PostLike();
            $model->user_id = $request->user->user_id;
            $model->post_id = $request->postId;
            $model->save();
            $post = Post::whereId($request->postId)->first();
            $count = $post->like_count + 1; 
            $post->update(['like_count' => $count]);
        }
        
    }
}
