<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
class Event extends Model {

    //constants
    const EVENT_FULL_IMG = '/images/event/';
    const EVENT_THUMB_IMG = '/images/event/thumb/';
    const UPCOMING = 'upcoming';
    const ALL = 'all';
    const INVITED = 'invited';
    const COMPLETE = 'complete';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function group() {
        return $this->belongsTo(Group::class);
    }

    /**
     * Update an event image.
     *
     * @param int
     * @param array
     */
    public static function updateImage($event_id, array $event_data) {
        Event::find($event_id)->update($event_data);
    }

    public static function getEvent($request, $user_id, $type) {
        $from = $request->page * env('EVENT_LIMIT', 10);
        $data = [];
        $firstQuery = Event::where('events.event_date', '!=', '');
        $firstQuery->join('group_follows', function($join) use($user_id) {
                    $join->on('group_follows.group_id', 'events.group_id')
						->on('events.event_type', DB::raw(0))
                            ->on('group_follows.user_id', DB::raw($user_id));
                })
                ->select("events.*");

        $query = Event::where('events.event_date', '!=', '')->select("events.*");
        $query->join('group_users', function($join) use($user_id) {
                    $join->on('group_users.group_id', 'events.group_id')
                            ->on('group_users.is_joined', DB::raw(2))
                            ->on('group_users.user_id', DB::raw($user_id));
                });
        if (Event::UPCOMING == $type) {
            $query->where('event_date', '>', Carbon::now()->timezone('UTC')->format('Y-m-d'));
        }
        $result = $query->union($firstQuery)->skip($from)
                        ->take(env('EVENT_LIMIT', 10))
                        ->get()->toArray();
        $count = $query->union($firstQuery)->get()->count();

        $data['data'] = $result;
        $data['per_page'] = env('EVENT_LIMIT', 10);
        $data['total_result'] = $count;
        return $data;
    }

}
