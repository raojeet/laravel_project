<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Group extends Model {

    //constants
    const GROUP_FULL_IMG = '/images/group/';
    const GROUP_THUMB_IMG = '/images/group/thumb/';
    const MY = 'my';
    const ALL = 'all';
    const POPULAR = 'popular';
    const FOLLOWING = 'following';
    const MYKEY = 'groupMy';
    const ALLKEY = 'groupAll';
    const POPULARKEY = 'groupPopular';
    const FOLLOWINGKEY = 'groupFollowing';
	const ACCEPTED = 1;
    const REJECTED = 2;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function groupFollow() {
        return $this->hasMany(GroupFollow::class);
    }

    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function groupUser() {
        return $this->hasMany(GroupUser::class);
    }
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function groupEvent() {
        return $this->hasMany(Event::class);
    }

    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function groupPost() {
        return $this->hasMany(Post::class);
    }

    /**
     * Update an group image.
     *
     * @param int
     * @param array
     */
    public static function updateImage($group_id, array $group_data) {
        Group::find($group_id)->update($group_data);
    }

    /**
     * 
     * @param type $request
     * 
     */
    public static function store($request) {
        if (!isset($request->groupId) && empty($request->groupId)) {
            $model = new Group();
        } else {
            $model = Group::find($request->groupId);
        }
        $model->title = $request->title;
        $model->group_description = $request->groupDescription;
        $model->tag_line = $request->tagLine;
        $model->city = $request->city;
        $model->state = $request->state;
        $model->user_id = $request->user_id;
        $model->save();
        return $model;
    }

    public static function getGroupList($request, $user_id) {
        switch ($request->groupType) {
            case Group::MY:
                
                $result[Group::MYKEY] = Group::getGroups($request, $user_id, Group::MY);
                break;
            case Group::FOLLOWING:
                $result[Group::FOLLOWINGKEY] = Group::getGroups($request, $user_id, Group::FOLLOWING);
                break;
            case Group::POPULAR:
                $result[Group::POPULARKEY] = Group::getGroups($request, $user_id, Group::POPULAR);
                break;
            default:
                $my = Group::getGroups($request, $user_id, Group::MY);
                $following = Group::getGroups($request, $user_id, Group::FOLLOWING);
                $popular = Group::getGroups($request, $user_id, Group::POPULAR);
                $result = [Group::MYKEY => $my,
                    Group::FOLLOWINGKEY => $following, Group::POPULARKEY => $popular];
                break;
        }
        return $result;
    }

    public static function getGroups($request, $user_id, $type) {
        $from = $request->page * env('GROUP_LIMIT', 10);
        $data = [];
        switch ($type) {
            case Group::MY:
                $query = Group::where('user_id',$user_id);
                break;
            case Group::FOLLOWING:
               $query = Group::join((new GroupFollow)->getTable(), 'group_follows.group_id', '=', 'groups.id')
                ->leftJoin((new GroupUser)->getTable(), 'group_users.group_id', '=', 'groups.id')
                ->select('groups.*',DB::raw('(CASE WHEN group_follows.user_id = "' . $user_id . '" THEN 1 ELSE 0 END) AS is_follow'), 'group_users.is_joined')
                ->where('group_follows.user_id',$user_id)->where('groups.is_admin_approved',true);
                break;
            case Group::POPULAR:
               $query = Group::leftJoin((new GroupFollow)->getTable(), 'group_follows.group_id', '=', 'groups.id')
                ->leftJoin((new GroupUser)->getTable(), 'group_users.group_id', '=', 'groups.id')
                ->select('groups.*',DB::raw('(CASE WHEN group_follows.user_id = "' . $user_id . '" THEN 1 ELSE 0 END) AS is_follow'), 'group_users.is_joined')
                ->where('groups.is_admin_approved',true)->orderBy('joined_count','DESC');
                break;
            default:
               $query = Group::leftJoin((new GroupFollow)->getTable(), 'group_follows.group_id', '=', 'groups.id')
                        ->leftJoin((new GroupUser)->getTable(), 'group_users.group_id', '=', 'groups.id')
                        ->select('groups.*',DB::raw('(CASE WHEN group_follows.user_id = "' . $user_id . '" THEN 1 ELSE 0 END) AS is_follow'), 'group_users.is_joined')
                        ->where('groups.is_admin_approved',true);
                break;
        }
        
        $count = $query->count();
        $result = $query->groupBy('groups.id')
                ->orderBy('groups.title', 'ASC')->skip($from)
                        ->take(env('GROUP_LIMIT', 10))
                        ->get()->toArray();
        if(count($result)>0){
           $data['data'] = $result; 
        } else{
            $data['data'] = [];
        }           
        
        $data['per_page'] = env('GROUP_LIMIT', 10);
        $data['total_result'] = $count;
        return $data;
    }

    public static function getGroupDetails($request){
        $userId = $request->user->user_id;
        $groupId = $request->groupId;
        $result = Group::with(['groupUser' => function ($query){
                        $query->join('users', 'users.id', '=', 'group_users.user_id' );
                    },'user','groupEvent','groupPost'])
                //->leftJoin((new GroupUser)->getTable(), 'group_users.group_id', '=', 'groups.id')
                ->leftJoin('group_users', function($join) use($userId,$groupId) {
                    $join->on('groups.id', 'group_users.group_id')
                            ->on('group_users.is_joined', DB::raw(2))
                            ->on('group_users.group_id', DB::raw($groupId))
                            ->on('group_users.user_id', DB::raw($userId));
                })
                ->leftJoin('group_follows', function($join) use($userId,$groupId) {
                    $join->on('groups.id', 'group_follows.group_id')
                            ->on('group_follows.group_id', DB::raw($groupId))
                            ->on('group_follows.user_id', DB::raw($userId));
                })
                //->leftJoin((new GroupFollow)->getTable(), 'group_follows.group_id', '=', 'groups.id')
                ->select('groups.*',DB::raw('(CASE WHEN group_follows.user_id = "' . $request->user->user_id . '" THEN 1 ELSE 0 END) AS is_follow'), DB::raw('(CASE WHEN group_users.is_joined IS NULL THEN 0 ELSE 1 END) AS is_joined'))
                ->where('groups.id',$request->groupId)
                ->get();
        return $result;
    }

    public static function  getMyGroupList($user_id){
        $query = Group::whereUserId($user_id);
        $result = $query->get()->toArray();
        return $result;
    }

    /**
     * function is used to fetch questions
     * @return object
     */
    public static function getGroupListToAdmin() {
        $data =  Group::select('id','title','tag_line','city','state')
                ->orderBy('created_at','DESC')
                ->take(env('GROUP_LIMIT',10))
                ->get();
        return ['groups' => $data]; 
    }
    
    
    /**
     * get user list Version V1
     * @param  $inputs
     * @return object
     */
    public static function groupList($inputs) {
        $search = $inputs->userName;
        $query = Group::select('groups.id','groups.is_admin_approved','groups.user_id','title','tag_line','groups.city','groups.state','users.name as name',DB::Raw('date(groups.created_at) as created_at'));
                $query->leftJoin((new User)->getTable(), 'users.id', '=', 'groups.user_id');
                if (!empty($search)) {
                    $query->where(function($query) use ($search) {
                        $query->where('title', 'LIKE', "%$search%")
                                ->orWhere('group_description', 'LIKE', "%$search%")
                                ->orWhere('tag_line', 'LIKE', "%$search%");
                    });
                }
        return $query->get();
    }


}
