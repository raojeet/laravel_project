<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFollow extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_follows';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $hidden = [ 'created_at','updated_at'];

    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    /**
     * function 
     * @param type $request
     */
    public static function followUser($request) {
        if(count($request->userId) > 0) {
            $userDataArray = array();
            foreach ($request->userId as $id) {
                $data[] = [ 'following_id' => $id,
                   'follower_id' => $request->user->user_id  
                ];
                $userDataArray[] = [ 'following_id' => $id,
                   'follower_id' => $request->user->user_id,   
                   'follower_name' => $request->user->name   
                ];
            }
            UserFollow::insert($data);
            return $userDataArray;
        }
        
    }
}
