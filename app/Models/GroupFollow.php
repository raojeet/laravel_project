<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupFollow extends Model {

    const FOLLOW = 1;
    const UNFOLLOW = 2;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group_follows';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $hidden = ['updated_at'];
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function group() {
        return $this->belongsTo(Group::class);
    }
    
    /**
     * function 
     * @param type $request
     */
    public static function follow($request) {
        if($request->status == GroupFollow::UNFOLLOW) {
            GroupFollow::whereUserId($request->user->user_id)->whereGroupId($request->groupId)->delete();
            $group = Group::whereId($request->groupId)->first();
            $count = $group->follow_count-1; 
            $group->update(['follow_count' => $count]);
        } else {
            $model = new GroupFollow();
            $model->user_id = $request->user->user_id;
            $model->group_id = $request->groupId;
            $model->save();
            $group = Group::whereId($request->groupId)->first();
            $count = $group->follow_count+1; 
            $group->update(['follow_count' => $count]);
        }
        
    }
}
