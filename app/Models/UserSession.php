<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Utility\Utility;

class UserSession extends Model {

    const IPHONE = 'IPHONE';
    const ANDROID = 'ANDROID';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_sessions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'session_id', 'device_token', 'device_type'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ 'timestamp'];

    /**
     * Disable timestamps fields
     */
    public $timestamps = false;

    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /** function is used to generate and save user token
     *
     * @param user id (int), device token (string)
     * @return result genrated token
     */
    public static function saveToken($user) {
        $token = Utility::generateToken();
        $data = [
            'user_id' => $user->user_id,
            'device_token' => (empty($user->deviceToken)) ? '123456df' : $user->deviceToken,
            'device_type' => $user->deviceType,
            'session_id' => $token
        ];

        $session = new UserSession();
        $session->fill($data);
        $session->save();
        return $token;
    }

    /** function is used to get user object
     *
     * @param session id (string)
     * @return user obejct with data
     */
    public static function getUserBySessionId($session_id) {
        return User::join((new UserSession)->getTable(), (new UserSession)->getTable().'.user_id', '=', 'users.id')
                        ->select('name','email', 'password', 'session_id', 'user_id','is_verify','is_email_verify')
                        ->where((new UserSession)->getTable().'.session_id', $session_id)
                        ->first();
    }

    /** function is used to autenticate user session 
     *
     * @param session id (string)
     * @return row count (int)
     */
    public static function checkAuthentication($session_id) {
        return UserSession::where('session_id', $session_id)->count();
    }
    
    /**
     * Updates a user session.
     *
     * @param int
     * @param array
     */
    public static function updateToken($session_id, array $user_data) {
        UserSession::whereSessionId($session_id)->update($user_data);
    }
}
