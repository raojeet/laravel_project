<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Notification extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'notifications';
    
    const NOTIFICATION_LIMIT = 40;
    const SENT = 1;
    const PROCESS = 2;
    const Notification_ALL = 'all_notifications';
    const Notification_INVITES = 'invites';
    const Notification_COMPLETE = 'complete';
    const USER_FOLLOW = 'Userfollow';
    const GROUP_FOLLOW = 'Groupfollow';
    const GROUP_JOIN = 'Groupjoin';
    const POST_CREATE = 'Createpost';
    const GROUP_POST_CREATE = 'Grouppostcreate';
    const POST_LIKE = 'Postlike';
    const POST_COMMENT = 'Postcomment';
    const GROUP_JOIN_ACCEPT = 'Groupjoinaccept';
    const GROUP_JOIN_REJECT = 'Groupjoinreject';
    const EVENT_CREATE = 'Createevent';
    
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'message', 'is_sent'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['updated_at'];
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function senderUser() {
        return $this->belongsTo(User::class);
    }

    /**
     * function is used to fetch questions
     * @param type $age
     * @return object
     */
    public static function getNotificationList($request, $user_id) {
        switch ($request->notificationType) {
            case Notification::Notification_ALL:
                $result[Notification::Notification_ALL] = Notification::getNotifications($request, $user_id, Notification::Notification_ALL);
                break;
            case Notification::Notification_INVITES:
                $result[Notification::Notification_INVITES] = Notification::getNotifications($request, $user_id, Notification::Notification_INVITES);
                break;
            default:
                $all = Notification::getNotifications($request, $user_id, Notification::Notification_ALL);
                $invites = Notification::getNotifications($request, $user_id, Notification::Notification_INVITES);
                $result = [Notification::Notification_ALL => $all,
                    Notification::Notification_INVITES => $invites];
                break;
        }
        return $result;
    }

    public static function getNotifications($request, $user_id, $type) {
        $from = $request->page * env('NOTIFICATION_LIMIT', 10);
        $data = [];
        switch ($type) {
            case Notification::Notification_ALL:
                $query = Notification::with('senderUser')->where('reciever_user_id',$user_id)->where('type',false);
                break;
            case Notification::Notification_INVITES:
               $query = Notification::with('senderUser')->where('reciever_user_id',$user_id)->where('type',true);
                break;
        }
        
        $count = $query->count();
        $result = $query->select('id as notificationId','sender_user_id','message','is_sent','is_read','created_at','title','group_id','group_status')
                    ->orderBy('id','DESC')
                    ->skip($from)
                    ->take(env('NOTIFICATION_LIMIT', 10))
                    ->get()->toArray();

        $data['data'] = $result;
        $data['per_page'] = env('NOTIFICATION_LIMIT', 10);
        $data['total_result'] = $count;
        return $data;
    }
    
    /**
     * function is used to fetch online user list for which notification is not yet sent
     * @param none
     * @return object
     */
    public static function getList() {
        return Notification::join('user_sessions as us', 'us.user_id', '=', 'notifications.reciever_user_id')
                        ->select('notifications.id','title','session_id', 'is_sent', 'us.device_type', 
                                'us.device_token',DB::raw('(SELECT COUNT(*) FROM notifications WHERE is_read = 0) AS badge'))
                        ->where('is_sent', 0)
                        ->where('device_token', '<>', env('DUMY_TOKEN','123456'))
                        ->offset(0)
                        ->limit(Notification::NOTIFICATION_LIMIT)
                        ->get();
    }
    
}
