<?php

namespace App\Models;

use App\Models\Group;
use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model {

    const PENDING = 1;
    const ACCEPTED = 2;
    const REJECTED = 3;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'group_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public $hidden = ['updated_at'];
    
    /** function used to set inverse has one (belongs to) eloquent relation
     * @param none
     * @return realtion
     */
    public function group() {
        return $this->belongsTo(Group::class);
    }
    
    /**
     * function 
     * @param type $request
     */
    public static function join($request) {
        if($request->status == GroupUser::REJECTED) {
            GroupUser::whereUserId($request->user->user_id)->whereGroupId($request->groupId)->delete();
        } else {
            $model = new GroupUser();
            $model->user_id = $request->user->user_id;
            $model->group_id = $request->groupId;
            $model->save();
        }
        
    }

    /**
     * function 
     * @param type $request
     */
    public static function acceptRequestGroup($request) {
        GroupUser::where('user_id',$request->senderUserId)->where('group_id',$request->groupId)->update(['is_joined'=>$request->status]);
        Group::where("id", $request->user->user_id)->increment("joined_count");
    }

    /**
     * function 
     * @param type $request
     */
    public static function rejectRequestGroup($request) {
        GroupUser::where('user_id',$request->senderUserId)->where('group_id',$request->groupId)->update(['is_joined'=>$request->status]);
    }
}
