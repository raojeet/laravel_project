<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class NotificationEvent {

    use SerializesModels;

    public $notification;
    public $action;

    /**
     * Create a new event instance.
     *
     * @param  notification  $notification
     * @return void
     */
    public function __construct($notification,$action) {
        $this->notification = $notification;
        $this->action = $action;
    }

}
