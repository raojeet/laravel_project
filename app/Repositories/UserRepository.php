<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Models\UserSession;

class UserRepository implements Contracts\UserRepositoryInterface {

    /**
     * Create a new UserRepository instance.
     *
     * @param  \App\Models\User $user
     * @return void
     */
    public function __construct(User $user) {
        $this->model = $user;
    }

    

    /**
     * Get’s a user by it’s email
     *
     * @param int
     * @return collection
     */
    public function get($phone) {
        return $this->model->wherePhone($phone)->first();
    }

     /**
     * Get’s a user by it’s email
     *
     * @param int
     * @return collection
     */
    public function getEmail($email) {
        return $this->model->whereEmail($email)->first();
    }
        
    public function signup($request) {
        $data = [
                'name' => $request->name,
                'password' => bcrypt($request->password),
                'username' => $request->userName,
                'email' => $request->email,
                'role' => $request->role
            ];
        $user = $this->get($request->phone);
        $this->update($user->id,$data);
    }
    
    public function verifyUser($request) {
        $user = $this->get($request->phone);
        $this->update($user->id, ['is_verify' => true, 'verification_otp' => null]);
    }
    
    public function getUser($request) {
        $user = $this->get($request->phone);
        if (!is_null($user) && Hash::check($request->password, $user->password)) {
            $request->user_id = $user->id;
            $token = UserSession::saveToken($request);
            return [
                'user' => $user,
                'sessionId' => $token
            ];
        }
        return false;
    }

    public function getProfile($request) {
        $user = $this->model->find($request->userId)->first();
        return [
            'userProfile' => $user
        ];
    }

    /**
     * Insert user Data
     *
     * @return mixed
     */
    public function insert($data) {
        $this->model->insert($data);
    }
    
    /**
     * Get’s all users.
     *
     * @return mailparse_determine_best_xfer_encoding(fp)
     */
    public function all() {
        return $this->model->all();
    }

    /**
     * Deletes a user.
     *
     * @param int
     */
    public function delete($user_id) {
        $this->model->destroy($user_id);
    }

    /**
     * Updates a user.
     *
     * @param int
     * @param array
     */
    public function update($user_id, array $user_data) {
        $this->model->find($user_id)->update($user_data);
        return $this->model->find($user_id);
    }

    /** function is used to save temporary password 
     *
     * @param string
     */
    public function tempPassword($user, $code) {

        $user->password = Hash::make($code);
        $user->is_password_reset = true;
        $user->save();

        ## delete all logged in user session
        UserSession::where('user_id', $user->id)->delete();
    }

    /**
     * Verify user email.
     *
     * @param string
     * @param void
     */
    public function verifyEmail($code) {
        $user = $this->model->where('verification_otp',$code)->first();
        if(!is_null($user)){
            $user->is_email_verify = true;
            $user->verification_otp = null;
            $user->save();
        }else{
            return false;
        }
        return true;
    }

}
