<?php

namespace App\Repositories\Contracts;

interface EventRepositoryInterface {

    /**
     * Get's a user by it's ID
     *
     * @param int
     */
    public function get($event_id);

    /**
     * Get's all users.
     *
     * @return mixed
     */
    public function all($request, $user_id);

    /**
     * Deletes a user.
     *
     * @param int
     */
    public function delete($event_id);
    
    
    /**
     * Insert a user.
     *
     * @param int
     */
    public function insert($event_data);
    
    
    /**
     * Updates a user.
     *
     * @param int
     * @param array
     */
    public function update($event_id, array $event_data);
}
