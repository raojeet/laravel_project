<?php

namespace App\Repositories\Contracts;

interface PostRepositoryInterface {

    /**
     * Get's a post by it's ID
     *
     * @param int
     */
    public function get($post_id);

    /**
     * Get's all users.
     *
     * @return mixed
     */
    public function all($request, $user_id);

    /**
     * Deletes a post.
     *
     * @param int
     */
    public function delete($post_id);
    
    
    /**
     * Insert a post.
     *
     * @param int
     */
    public function insert($post_data);
    
    
    /**
     * Updates a post.
     *
     * @param int
     * @param array
     */
    public function update($post_id, array $post_data);
}
