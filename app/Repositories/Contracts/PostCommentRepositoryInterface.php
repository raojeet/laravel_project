<?php

namespace App\Repositories\Contracts;

interface PostCommentRepositoryInterface {

    /**
     * Get's a post by it's ID
     *
     * @param int
     */
    public function get($post_id);

    /**
     * Get's all users.
     *
     * @return mixed
     */
    public function all($request);

    /**
     * Deletes a post.
     *
     * @param int
     */
    public function delete($post_comment_id);
    
    
    /**
     * Insert a post.
     *
     * @param int
     */
    public function insert($post_comment_data);
    
    
    /**
     * Updates a post.
     *
     * @param int
     * @param array
     */
    public function update($post_id, array $post_comment_data);
}
