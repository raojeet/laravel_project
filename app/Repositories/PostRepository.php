<?php

namespace App\Repositories;

use App\Models\Post;
use Illuminate\Support\Facades\DB;

class PostRepository implements Contracts\PostRepositoryInterface {

    /**
     * Create a new UserRepository instance.
     *
     * @param  \App\Models\User $post
     * @return void
     */
    public function __construct(Post $post) {
        $this->model = $post;
    }

    /**
     * Get’s a event by it’s email
     *
     * @param int
     * @return collection
     */
    public function get($id) {
        return $this->model->find($id);
    }

    /**
     * Insert event Data
     *
     * @return mixed
     */
    public function insert($data) {
        if (!empty($data->postId)) {
            $this->model = Post::find($data->postId);
        }
        $this->model->post_type = $data->postType;
        $this->model->location = $data->location;
        $this->model->latitude = $data->latitude;
        $this->model->longitude = $data->longitude;
        $this->model->post_description = $data->postDescription;
        $this->model->user_id = $data->user_id;
        $this->model->group_id = $data->groupId;
        $this->model->save();
        return ['post' => $this->model];
    }

    /**
     * Get’s all users.
     *
     * @return mailparse_determine_best_xfer_encoding(fp)
     */
    public function all($request, $user_id) {
        $from = $request->page * env('EVENT_LIMIT', 10);
        $query = $this->model->with(['postComment' => function ($query) {
                        $query->join('users', 'users.id', '=', 'post_comments.user_id');
                    }, 'user', 'postImage', 'postLike' => function ($query) {
                        $query->join('users', 'users.id', '=', 'post_likes.user_id');
                    }])
                ->leftJoin('post_likes', 'post_likes.post_id', '=', 'posts.id')
                ->select('posts.*','groups.title as group_name', DB::raw('(CASE WHEN post_likes.post_id IS NULL THEN 0 ELSE 1 END) AS is_like'))
                ->leftJoin('group_follows', function($join) use($user_id) {
                    $join->on('posts.group_id', 'group_follows.group_id')
                            ->on('group_follows.user_id', DB::raw($user_id));
                })
                ->leftJoin('user_follows', function($join) use($user_id) {
                    $join->on('posts.user_id', DB::raw('user_follows.following_id'))
                        ->on('user_follows.follower_id', DB::raw($user_id));
                })->where(function($q) use($user_id) {
                    $q->whereRaw("posts.group_id = group_follows.group_id")
                            ->orWhere(DB::raw('posts.user_id'), DB::raw('user_follows.following_id'))
                            ->orWhere('posts.user_id', DB::raw($user_id));
                })
                ->leftJoin('groups', 'groups.id', '=', 'posts.group_id');
        $count = $query->count();

        $data = $query->groupBy('posts.id')
                ->orderBy('posts.updated_at', 'DESC')
                ->orderBy('posts.id', 'DESC')
                ->skip($from)
                ->take(env('EVENT_LIMIT', 10))
                ->get();
        return [
            'posts' => $data,
            'per_page' => env('EVENT_LIMIT', 10),
            'total_result' => $count
        ]; 
    }

    /**
     * Deletes a event.
     *
     * @param int
     */
    public function delete($post_id) {
        $this->model->destroy($post_id);
    }

    /**
     * Updates a user.
     *
     * @param int
     * @param array
     */
    public function update($post_id, array $post_data) {
        $this->model->find($post_id)->update($post_data);
    }

}
