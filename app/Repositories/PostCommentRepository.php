<?php

namespace App\Repositories;

use App\Models\PostComment;
use App\Models\Post;
class PostCommentRepository implements Contracts\PostCommentRepositoryInterface {

    /**
     * Create a new UserRepository instance.
     *
     * @param  \App\Models\User $postComment
     * @return void
     */
    public function __construct(PostComment $postComment) {
        $this->model = $postComment;
    }

    
    /**
     * Get’s a event by it’s email
     *
     * @param int
     * @return collection
     */
    public function get($id) {
        return $this->model->find($id);
    }


    /**
     * Insert event Data
     *
     * @return mixed
     */
    public function insert($data) {
        //echo "here";die;
        //echo '<pre>';print_r($data);die;
        // if(!empty($data->postCommentId)) {
        //    $this->model = PostComment::find($data->postCommentId); 
        // }
        $this->model->comment = $data->comment;
        $this->model->user_id = $data->user->user_id;
        $this->model->post_id = $data->postId;
        $this->model->save();

        $post = Post::whereId($data->postId)->first();
        $count = $post->comment_count+1; 
        $post->update(['comment_count' => $count]);

        return ['postComment' => $this->model];
        
    }
    
    /**
     * Get’s all users.
     *
     * @return mailparse_determine_best_xfer_encoding(fp)
     */
    public function all($request) {
        $from = $request->page * env('EVENT_LIMIT', 10);
        $count = $this->model->wherePostId($request->postId)->count();
        $query = $this->model->wherePostId($request->postId);
        $data = $query->join('users', 'users.id', '=', 'post_comments.user_id')
                ->select('post_comments.*','users.name')
                ->skip($from)
                ->take(env('EVENT_LIMIT', 10))
                ->get();
        return [
            'postComments' => $data,
            'per_page' => env('EVENT_LIMIT', 10),
            'total_result' => $count
        ];
    }

    /**
     * Deletes a event.
     *
     * @param int
     */
    public function delete($data) {
        $this->model->destroy($data->postCommentId);

        $post = Post::whereId($data->postId)->first();
        $count = $post->comemnt_count-1; 
        $post->update(['comment_count' => $count]);
    }

    /**
     * Updates a user.
     *
     * @param int
     * @param array
     */
    public function update($post_comment_id, array $post_comment_data) {
        $this->model->find($post_comment_id)->update($post_comment_data);
    }

}
