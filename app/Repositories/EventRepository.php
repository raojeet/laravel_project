<?php

namespace App\Repositories;

use App\Models\Event;
use Illuminate\Support\Facades\DB;
class EventRepository implements Contracts\EventRepositoryInterface {

    /**
     * Create a new UserRepository instance.
     *
     * @param  \App\Models\Event $event
     * @return void
     */
    public function __construct(Event $event) {
        $this->model = $event;
    }

    /**
     * Get’s a event by it’s email
     *
     * @param int
     * @return collection
     */
    public function get($id) {
        return $this->model->find($id);
    }


    /**
     * Insert event Data
     *
     * @return mixed
     */
    public function insert($data, $id = false) {
        if(!empty($id)) {
           $this->model = Event::find($id); 
        }
        $this->model->title = $data->title;
        $this->model->event_description = $data->eventDescription;
        $this->model->event_date = $data->eventDate;
        $this->model->event_time = $data->eventTime;
        $this->model->venue = $data->location;
        $this->model->user_id = $data->user_id;
        $this->model->latitude = $data->latitude;
        $this->model->longitude = $data->longitude;
        $this->model->group_id = $data->groupId;
        $this->model->event_type = $data->eventType;
        $this->model->save();
        return ['event' => $this->model];
        
    }
    
    /**
     * fetch all Moments.
     *
     * @return mixed
     */
    public function all($request, $user_id) {
       
        switch ($request->eventType) {
            case Event::UPCOMING:
                $result[Event::UPCOMING] = Event::getEvent($request, $user_id, Event::UPCOMING);
               break;
            case Event::ALL:
                $result[Event::ALL] = Event::getEvent($request, $user_id, Event::ALL);
               break;
            case Event::INVITED:
               $result[Event::INVITED] = Event::getEvent($request, $user_id, Event::INVITED);
               break;
            default:
                $invited = Event::getEvent($request, $user_id,Event::INVITED);
                $all = Event::getEvent($request, $user_id,Event::ALL);
                $upcoming = Event::getEvent($request, $user_id,Event::UPCOMING);
                $result = [Event::INVITED => $invited,
                    Event::ALL => $all,
                    Event::UPCOMING => $upcoming];
                break;
        }
        return $result;
    }

    /**
     * Deletes a event.
     *
     * @param int
     */
    public function delete($event_id) {
        $this->model->destroy($event_id);
    }

    /**
     * Updates a event.
     *
     * @param int
     * @param array
     */
    public function update($event_id, array $event_data) {
        $this->model->find($event_id)->update($event_data);
    }

}
