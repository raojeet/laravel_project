<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class PostCommentServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        $this->app->bind('App\Repositories\Contracts\PostCommentRepositoryInterface',
                'App\Repositories\PostCommentRepository'
        );
    }

}
