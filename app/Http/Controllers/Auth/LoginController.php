<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Handle a login request to the application
     *
     * @param  \App\Http\Requests\LoginRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function login(LoginRequest $request) {
        $login = 'login';
        if ($lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            
            $redirect =  redirect($login)
                ->with('error', trans('admin/login.maxattempt'))
                ->withInput($request->only('email'));
        } else {
            $logValue = $request->input('email');

            $logAccess = filter_var($logValue, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

            $credentials = [
                $logAccess => $logValue,
                'password' => $request->input('password'),
            ];

            if (!auth()->validate($credentials)) {
                if (! $lockedOut) {
                    $this->incrementLoginAttempts($request);
                }
                return redirect($login)
                                ->with('error', trans('admin/login.credentials'))
                                ->withInput($request->only('email'));
            }

            $user = auth()->getLastAttempted();

            if ($user->role) {
                if (! $lockedOut) {
                    $this->incrementLoginAttempts($request);
                }
                auth()->login($user, $request->has('memory'));

                if ($request->session()->has('user_id')) {
                    $request->session()->forget('user_id');
                }

                return redirect('home');
            }
            $redirect = redirect($login)->with('error', trans('admin/login.app'));
        }
        return $redirect;
    }

}
