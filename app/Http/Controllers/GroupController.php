<?php

/**
 * group controller (web)
 *
 * This controller handles authenticating users for the application
 * and provide user details.
 *
 * @class      GroupController
 * @author     Jeevesh <author@example.com>
 * @version    Release: v1
 */

namespace App\Http\Controllers;

use App\Models\Group;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\groupRequest;
use Carbon\Carbon;
class GroupController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Group Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles all user related functionlities.
      | like group list, user profile, change password etc.
      | and it uses Yajra datatable service for user list.
      |
     */
    
    
    /**
     * function is used to fecth group list.
     * @param  $request
     * @return void
     */
    public function groupList(Request $request) {
        $groups = Group::groupList($request);
        return Datatables::of($groups)
                ->editColumn('name', function ($groups) {
                            $groups->url = url('/bikers/getUserDetails/' . $groups->user_id);
                            return view('partials.link', ['link' => $groups]);
                        })
                ->editColumn('created_at', function ($groups) {
                            return with(new Carbon($groups->created_at))->format('d-m-Y');
                        })
                ->addColumn('action', function ($group) {
                             return (string) view('partials.action', ['group' => $group,'edit' => true]);
                         })
                        ->rawColumns(['name','action'])
                        ->make(true);
    }
    
    /**
     * function is used to delete group.
     * @param  $request
     * @return void
     */
    public function approve(Request $request) {
        $id = (empty($request->id)) ? '' : $request->id; 
        $group_data['is_admin_approved'] = 1;
        Group::find($id)->update($group_data); 
        return "true";
    }

}
