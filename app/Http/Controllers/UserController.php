<?php

/**
 * user controller (web)
 *
 * This controller handles authenticating users for the application
 * and provide user details.
 *
 * @class      UserController
 * @author      <author@example.com>
 * @version    Release: v1
 */

namespace App\Http\Controllers;

use App\Repositories\Contracts\UserRepositoryInterface;
use App\Http\Requests\UpdatePasswordRequest;
use Illuminate\Support\Facades\Redirect;
use App\Models\User;
use App\Models\BikeImage;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Carbon\Carbon;

class UserController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | User Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles all user related functionlities.
      | like user list, user profile, change password etc.
      | and it uses Yajra datatable service for user list.
      |
     */

    protected $user;

    public function __construct(UserRepositoryInterface $user) {
        $this->user = $user;
    }

    /**
     * function is used to update user password to new one.
     * @param  $request
     * @return void
     */
    public function changePassword(UpdatePasswordRequest $request) {

        if (!\Auth::validate(['email' => \Auth::user()->email, 'password' => $request->old_password])) {
            return Redirect::to('password/change')->with('error', trans('admin/password.invalid_old_password'));
        }
        $this->user->updatePassword($request->password);

        return Redirect::to('password/change')->with('success', trans('admin/password.password_changed'));
    }

    /**
     * function is used to fecth user list.
     * @param  $request
     * @return void
     */
    public function userList(Request $request) {
        $user_data = User::userList($request);
        return Datatables::of($user_data)
                        ->addColumn('action', function ($user_data) {
                            return (string) view('partials.action', ['event' => $user_data]);
                        })
                        ->editColumn('created_at', function ($user_data) {
                            return with(new Carbon($user_data->created_at))->format('d-m-Y');
                        })
                        ->make(true);
    }

    /**
     * function is used to fecth user list.
     * @param  $request
     * @return void
     */
    public function bikerList(Request $request) {
        $user_data = User::bikerList($request);
        return Datatables::of($user_data)
                        ->addColumn('action', function ($user_data) {
                            return (string) view('partials.action', ['event' => $user_data]);
                        })
                        ->editColumn('name', function ($user_data) {
                            $user_data->url = url('/bikers/getUserDetails/' . $user_data->id);
                            return view('partials.link', ['link' => $user_data]);
                        })
                        ->editColumn('created_at', function ($user_data) {
                            return with(new Carbon($user_data->created_at))->format('d-m-Y');
                        })
                        ->rawColumns(['name','action'])
                        ->make(true);
    }
    
    /**
     * function is used to get user profile
     * 
     * @param  $request (user id int)
     * @return object
     */
    public function profile(Request $request) {
        try {
            $user_data = $this->user->getUser($request->id);
            if (is_null($user_data)) {
                return Redirect::to('user');
            }
        } catch (\Exception $e) {
            return Redirect::to('user');
        }
        $profile = $user_data->toArray();
        $profile['created_at'] = with(new Carbon($user_data->created_at))->format('d-m-Y');
        return view('user.profile', compact('profile'));
    }

    public function getUserDetails($id){
        $profile = User::with('bike')->find($id);
        foreach($profile['bike'] as $bikes){
            $bike_images = BikeImage::where('bike_id',$bikes['id'])->get();
            $bikes['bikeImages'] = $bike_images;
        }
        return view('bikers.view', compact('profile'));
    }

}
