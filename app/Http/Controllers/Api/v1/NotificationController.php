<?php

namespace App\Http\Controllers\Api\v1;

use App\Utility\Utility;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Notification;

Class NotificationController extends \App\Http\Controllers\Controller {
    /*
      |--------------------------------------------------------------------------
      | Notification Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles users notifications for the application.
      |
     */

    public function __construct() {
        $this->utility = new Utility();
    }


    /**
     * function is used to fetch notification list
     * @param none 
     * @return response json
     */
    public function getNotification(\App\Http\Requests\v1\NotificationRequest $request) {
        try {
            $data = Notification::getNotificationList($request,$request->user->user_id);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'),$data);
       } catch (\Exception $e) {
           Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
           return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
       }
    }

}
