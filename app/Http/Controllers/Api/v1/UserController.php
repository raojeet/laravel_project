<?php

/**
 * user controller
 *
 * This controller handles authenticating users for the application
 * and provide user details for use it also register new users via email
 *
 * @class      UserController
 * @author     Jeet <author@example.com>
 * @version    Release: v1
 */

namespace App\Http\Controllers\Api\v1;

use App\Utility\Utility;
use App\Http\Requests\v1\SignupRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Http\Requests\v1\SigninRequest;
use Illuminate\Http\Request;
use App\Http\Requests\v1\OtpRequest;
use App\Http\Requests\v1\VerifyOtpRequest;
use App\Http\Requests\v1\ResetPasswordRequest;
use App\Http\Requests\v1\UpdateProfileRequest;
use App\Http\Requests\v1\GetProfileRequest;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\Mailing;
use App\Models\ResetPasswordLimit;

Class UserController extends \App\Http\Controllers\Controller {
    /*
      |--------------------------------------------------------------------------
      | User Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | provide user details for use it also register new users via email.
      |
     */

    protected $user;

    public function __construct(UserRepositoryInterface $user) {
        $this->utility = new Utility();
        $this->user = $user;
    }
    
    /**
     * function is used to send signup otp
     * @param $request 
     * @return response json
     */
    public function sendOtp(OtpRequest $request) {
        try {
            $otp = rand(1000,9999);
            $data  = ['phone' => $request->phone, 'verification_otp' => 1234];
            $this->user->insert($data);

            $content_message = "Dear User, Your One Time Password OTP for Bikes N Buddies App is ".$otp;
            $dlr_url = "http://bikesnbuddies.com/dev/public/";
            $api_url = "https://alerts.solutionsinfini.com/api/v3/index.php?api_key=A929521396xxxxxxxxxxxxxxxxxxxxxxxxx&method=sms&message=".urlencode($content_message)."&to=".$request->phone."&sender=BIKENB&
format=json"; 
            //$response = file_get_contents($api_url);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.otp_sent'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to fetch validate signup
     * @param $request 
     * @return response json
     */
    public function signup(SignupRequest $request) {
        try {
            $this->user->signup($request);
            
            if(!empty($request->email)){
                $token = rand(1000,9999);
                $user_model = User::where('phone', $request->phone)->first();
                $user_model->verification_otp=$token;
                $user_model->save();
                $mail_data = ['view' => 'signup', 'subject' => trans('api.signup_subject'), 'url' => 'api/v1/user/verifyEmail/'.$token,'name' => $request->name];
                Mail::to($request->email)->send(new Mailing($mail_data));
            }

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to send signup otp
     * @param $request 
     * @return response json
     */
    public function verifyOtp(VerifyOtpRequest $request) {
        try {
            $this->user->verifyUser($request);

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.otp_verified'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }

    /**
     * function is used to signin
     * @param $request 
     * @return response json
     */
    public function signin(SigninRequest $request) {
        try {
            $user_data = $this->user->getUser($request);
            
            if (is_null($user_data) || empty($user_data)) {
                return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.invalid_password'));
            }
            if (isset($user_data['user']) && empty($user_data['user']->is_email_verify)) {
                return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.email_not_verified'));
            }
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'), $user_data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to update user profile
     * @param $request 
     * @return response json
     */
    public function updateProfile(UpdateProfileRequest $request) {
        try {            
            $data = ['dob' => $request->dob,
                'gender' => $request->gender,
                'city' => $request->city,
                'country' => $request->country,
                'email' => $request->email,
                'name' => $request->name,
                "post_code" => $request->postCode,
                "role" => $request->role,
                "location" => $request->location ];

            $user_email = User::where('email',$request->email)->first();
            $user_data = $this->user->update($request->user->user_id,$data);
            $user['user'] = $user_data->toArray();  
            
            if(empty($user_email)){
                $token = rand(1000,9999);
                $this->user->update($request->user->user_id,array('verification_otp'=>$token));
                $mail_data = ['view' => 'signup', 'subject' => trans('api.signup_subject'), 'url' => 'api/v1/user/verifyEmail/'.$token,'name' => $user_data['name']];
                Mail::to($request->email)->send(new Mailing($mail_data));
            }
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'), $user);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to to logout
     * @param $request 
     * @return response success
     */
    public function logout(Request $request) {
        try {
            \App\Models\UserSession::whereSessionId($request->header('sessionId'))->delete();
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to save image
     * @param $request 
     * @return response json
     */
    public function saveImage(\App\Http\Requests\v1\ImageRequest $request) {
        try {
            if(!empty($request->eventId)) {
                EventController::saveImage($request->eventId, $request->file('image'));
            }
            else if(!empty($request->postId)) {
                    PostController::saveImage($request->postId, $request->file('image'));
            } else {
                $image = Utility::saveImage($request->file('image'), public_path().User::USER_FULL_IMG, public_path().User::USER_THUMB_IMG);
                $img_path = url(User::USER_FULL_IMG.$image);
                $this->user->update($request->user->user_id, ['image' => $img_path]);
            }
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to update user device token
     * @param $request 
     * @return response json
     */
    public function updateDeviceToken(\App\Http\Requests\v1\UpdateDeviceRequest $request) {
        try {
            \App\Models\UserSession::updateToken($request->user->session_id, ['device_token' => $request->deviceToken]);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to signin
     * @param $request 
     * @return response json
     */
    public function getProfile(GetProfileRequest $request) {
        try {
            $user_data = $this->user->getProfile($request);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'), $user_data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }

    /**
     * function is used to send temporary password on users email
     * @param $request 
     * @return response
     */
    public function resetPassword(ResetPasswordRequest $request) {
        try {
            $user_data = $this->user->getEmail($request->email);
            $status = ResetPasswordLimit::resetLimit($user_data->id);
            if (empty($status) || $status === false) {
                return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('passwords.limit_exhausted'));
            }
            $random_code = Utility::randomString();
            $this->user->tempPassword($user_data, $random_code);

            $mail_data = ['view' => 'resetPassword', 'subject' => trans('api.reset_password_subject'), 'password' => $random_code,'user_name' => $user_data['name']];
            Mail::to($request->email)->send(new Mailing($mail_data));
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.reset_pass_mail_sent'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }

    /**
     * function is used verify user email.
     * @param  string $verificaton_code
     * @return void
     */
    public function verifyEmail($verificaton_code) {
        $result_verify = $this->user->verifyEmail($verificaton_code);
        return view('confirm')->with('result_verify', $result_verify);
    }
}
