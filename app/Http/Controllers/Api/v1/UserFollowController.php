<?php

/**
 * user Follow controller
 *
 * This controller handles authenticating users for the application
 * and provide user details for use it also register new users via email
 *
 * @class      UserController
 * @author     Jeet <author@example.com>
 * @version    Release: v1
 */

namespace App\Http\Controllers\Api\v1;

use App\Utility\Utility;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\v1\ContactRequest;
use App\Http\Requests\v1\UserFollowRequest;
use App\Models\User;
use App\Events\NotificationEvent;
use App\Models\UserFollow;
use App\Models\Notification;

Class UserFollowController extends \App\Http\Controllers\Controller {
    /*
      |--------------------------------------------------------------------------
      | User follow Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | provide user details for use it also register new users via email.
      |
     */

    public function __construct() {
        $this->utility = new Utility();
    }
    
    /**
     * function is used to send contact list
     * @param $request 
     * @return response json
     */
    public function followList(ContactRequest $request) {
        try {
            $users = User::getContact($request);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'),$users);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to follow user
     * @param $request 
     * @return response json
     */
    public function followUser(UserFollowRequest $request) {
        try {
            $data = UserFollow::followUser($request);
            if(count($data)>0) {
                event(new NotificationEvent($data,Notification::USER_FOLLOW));
            }
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
}
