<?php

/**
 * post controller
 *
 * This controller handles authenticating users for the application
 * and provide user details for use it also register new users via email
 *
 * @class      UserController
 * @author     Appster <author@example.com>
 * @version    Release: v1
 */

namespace App\Http\Controllers\Api\v1;

use App\Utility\Utility;
use App\Http\Requests\v1\PostCommentRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Repositories\Contracts\PostCommentRepositoryInterface;
use App\Events\NotificationEvent;
use App\Models\Notification;
use App\Models\Post;

Class PostCommentController extends \App\Http\Controllers\Controller {
    /*
      |--------------------------------------------------------------------------
      | Post Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | provide post details for use it also register new users via email.
      |
     */

    protected $postComment;

    public function __construct(PostCommentRepositoryInterface $postComment) {
        $this->utility = new Utility();
        $this->postComment = $postComment;
    }

    /**
     * function is used to add post
     * @param $request 
     * @return response json
     */
    public function addPostComment(PostCommentRequest $request) {
        try {
            $request->user_id = $request->user->user_id;
            $comment_data = $this->postComment->insert($request);

            $data = array();
            $postAdminData = Post::where('id',$request->postId)->where('user_id','<>',$request->user->user_id)->first();
            if(!is_null($postAdminData)){
               $data[] = [ 'following_id' => $postAdminData->user_id,
                   'follower_id' => $request->user->user_id,  
                   'follower_name' => $request->user->name,    
                ];
            }
            if(count($data)>0) {
                event(new NotificationEvent($data,Notification::POST_COMMENT));
            }

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.post_comment_added'), $comment_data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    
    /**
     * function is used to delete post
     * @param $request 
     * @return response json
     */
    public function deletePostComment(\App\Http\Requests\v1\DeletePostCommentRequest $request) {
        try { 
            $this->postComment->delete($request);

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.post_comment_delete'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }

    /**
     * function is used to event list
     * @param $request 
     * @return response json
     */
    public function postCommentList(\App\Http\Requests\v1\PostCommentListRequest $request) {
        try {
            $data = $this->postComment->all($request);
            
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'),$data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }

}
