<?php

/**
 * post controller
 *
 * This controller handles authenticating users for the application
 * and provide user details for use it also register new users via email
 *
 * @class      UserController
 * @author     Appster <author@example.com>
 * @version    Release: v1
 */

namespace App\Http\Controllers\Api\v1;

use App\Utility\Utility;
use App\Http\Requests\v1\PostRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Repositories\Contracts\PostRepositoryInterface;
use App\Models\Post;
use App\Models\GroupFollow;
use App\Models\GroupUser;
use App\Models\Group;
use App\Models\UserFollow;
use App\Events\NotificationEvent;
use App\Models\Notification;

Class PostController extends \App\Http\Controllers\Controller {
    /*
      |--------------------------------------------------------------------------
      | Post Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | provide post details for use it also register new users via email.
      |
     */

    protected $post;

    public function __construct(PostRepositoryInterface $post) {
        $this->utility = new Utility();
        $this->post = $post;
    }

    /**
     * function is used to add post
     * @param $request 
     * @return response json
     */
    public function addPost(PostRequest $request) {
        try {
            $request->user_id = $request->user->user_id;
            $data = $this->post->insert($request);

            // code to send notification
            if(!empty($request->groupId)){
                
                // send notification to all group following members
                $groupFollowData = array();
                $groupUserFollowsData = GroupFollow::where('group_id',$request->groupId)->get();
                if(count($groupUserFollowsData)>0){
                    foreach($groupUserFollowsData as $groupUserFollows){
                        $groupFollowData[] = [  'following_id' => $groupUserFollows->user_id,
                                'follower_name' => $request->user->name,
                                'follower_id' => $request->user->user_id    
                            ];
                    }
                }
                if(count($groupFollowData)>0) {
                    event(new NotificationEvent($groupFollowData,Notification::GROUP_POST_CREATE));
                }

                // send notification to all group joined members
                $groupUserData = array();
                $groupUserJoinesData = GroupUser::where('group_id',$request->groupId)->get();
                if(count($groupUserJoinesData)>0){
                    foreach($groupUserJoinesData as $groupUserJoinesData){
                        $groupUserData[] = [  'following_id' => $groupUserJoinesData->user_id,
                                'follower_name' => $request->user->name,
                                'follower_id' => $request->user->user_id    
                            ];
                    }
                }
                if(count($groupUserData)>0) {
                    event(new NotificationEvent($groupUserData,Notification::GROUP_POST_CREATE));
                }

            }elseif(!empty($request->user->user_id)){
                // Send notification to user followers
                $data = array();
                $userFollowsData = UserFollow::where('following_id',$request->user->user_id)->get();
                if(count($userFollowsData)>0){
                    foreach($userFollowsData as $userFollows){
                        $data[] = [  'following_id' => $userFollows->follower_id,
                                'follower_name' => $request->user->name,
                                'follower_id' => $request->user->user_id    
                            ];
                    }
                }
                if(count($data)>0) {
                    event(new NotificationEvent($data,Notification::POST_CREATE));
                }
            }
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.post_added'), $data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to edit post
     * @param $request 
     * @return response json
     */
    public function EditPost(PostRequest $request) {
        try {
            $request->user_id = $request->user->user_id;
            $data = $this->post->insert($request);

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.post_update'), $data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to delete post
     * @param $request 
     * @return response json
     */
    public function deletePost(\App\Http\Requests\v1\DeletePostRequest $request) {
        try { 
            $post = $this->post->get($request->postId);
            if(!empty($post->post_url)) {
                $path = public_path().Post::POST_FULL_IMG;  
                @unlink($path.$post->post_url);
            }
            $this->post->delete($request->postId);

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.post_delete'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }

    /**
     * function is used to event list
     * @param $request 
     * @return response json
     */
    public function postList(\App\Http\Requests\v1\PostListRequest $request) {
        try {
            $data = $this->post->all($request,$request->user->user_id);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'),$data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }

    /**
     * function is used to save post image
     * @param $request 
     * @return response json
     */
    public static function saveImage($id, $image) {
        $img = Utility::saveImage($image, public_path() . Post::POST_FULL_IMG, public_path() . Post::POST_THUMB_IMG);
        $img_path = url(Post::POST_FULL_IMG.$img);
        $thumb_path = url(Post::POST_THUMB_IMG.$img);
        Post::updateImage($id, ['image' => $img_path,'thumb' => $thumb_path,'file_name' => $img]);
    }

    /**
     * function is used to save image
     * @param $request 
     * @return response json
     */
    public function saveVideo(\App\Http\Requests\v1\VideoRequest $request) {
        try {
            $video_file = $request->file('video');
            $video = Utility::saveVideo($video_file,public_path() . Post::POST_FULL_IMG, $video_file);
            $video_path = url(Post::POST_FULL_IMG.$video);

            $data = Post::updateVideo($request->postId, ['full_url' => $video_path,'file_name' => $video]);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'), $data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }

    /**
     * function is used to save image
     * @param $request 
     * @return response json
     */
    public function saveVideoThumb(\App\Http\Requests\v1\VideoThumbRequest $request) {
        try{
                $image = $request->file('videoThumb');
                $img = Utility::saveImage($image, public_path() . Post::POST_THUMB_IMG, '');
                $thumb_path = url(Post::POST_THUMB_IMG.$img);

                Post::updateVideoThumb($request->refrenceId, ['thumb_url' => $thumb_path]);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }

    /**
     * function is used to like post
     * @param $request 
     * @return response json
     */
    public function likePost(\App\Http\Requests\v1\PostLikeRequest $request) {
        try {
            $count = \App\Models\PostLike::wherePostId($request->postId)
                    ->whereUserId($request->user->user_id)->count();
            
            if($count > 0 && ($request->status == \App\Models\PostLike::LIKE)) {
               return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.already_liked'));  
            }
            \App\Models\PostLike::like($request);
            if(($request->status == \App\Models\PostLike::LIKE)){
				$data = array();
				$postAdminData = Post::where('id',$request->postId)->where('user_id','<>',$request->user->user_id)->first();
				if(!is_null($postAdminData)){
					$data[] = [ 'following_id' => $postAdminData->user_id,
                     'follower_id' => $request->user->user_id,  
                     'follower_name' => $request->user->name,    
					];
				}
				if(count($data)>0) {
				  event(new NotificationEvent($data,Notification::POST_LIKE));
				}
				
				// if group post like 
				if(!empty($request->groupId)){
					$group_post_data = array();
					$postGroupAdminData = Group::where('id',$request->groupId)->first();
					if(!is_null($postGroupAdminData)){
						$group_post_data[] = [ 'following_id' => $postGroupAdminData->user_id,
						 'follower_id' => $request->user->user_id,  
						 'follower_name' => $request->user->name,    
						];
					}
					if(count($group_post_data)>0) {
					  event(new NotificationEvent($group_post_data,Notification::POST_LIKE));
					}
				}
            }
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
}
