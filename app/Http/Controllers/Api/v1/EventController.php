<?php

/**
 * Event controller
 *
 * This controller handles authenticating users for the application
 * and provide user details for use it also register new users via email
 *
 * @class      EventController
 * @author     Jeet <author@example.com>
 * @version    Release: v1
 */

namespace App\Http\Controllers\Api\v1;

use App\Utility\Utility;
use App\Http\Requests\v1\EventRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Repositories\Contracts\EventRepositoryInterface;
use App\Models\Event;
use App\Models\GroupUser;
use App\Models\GroupFollow;
use App\Events\NotificationEvent;
use App\Models\Notification;

Class EventController extends \App\Http\Controllers\Controller {
    /*
      |--------------------------------------------------------------------------
      | Event Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | provide user details for use it also register new users via email.
      |
     */

    protected $event;

    public function __construct(EventRepositoryInterface $event) {
        $this->utility = new Utility();
        $this->event = $event;
    }

    /**
     * function is used to add event
     * @param $request 
     * @return response json
     */
    public function addEvent(EventRequest $request) {
        try {
            $request->user_id = $request->user->user_id;
            $event_data = $this->event->insert($request);


            // send notification to all group following members
                $groupFollowData = array();
                $groupUserFollowsData = GroupFollow::where('group_id',$request->groupId)->get();
                if(count($groupUserFollowsData)>0){
                    foreach($groupUserFollowsData as $groupUserFollows){
                        $groupFollowData[] = [  'following_id' => $groupUserFollows->user_id,
                                'follower_name' => $request->user->name,
                                'follower_id' => $request->user->user_id    
                            ];
                    }
                }
                if(count($groupFollowData)>0) {
                    event(new NotificationEvent($groupFollowData,Notification::EVENT_CREATE));
                }

                // send notification to all group joined members
                $groupUserData = array();
                $groupUserJoinesData = GroupUser::where('group_id',$request->groupId)->get();
                if(count($groupUserJoinesData)>0){
                    foreach($groupUserJoinesData as $groupUserJoinesData){
                        $groupUserData[] = [  'following_id' => $groupUserJoinesData->user_id,
                                'follower_name' => $request->user->name,
                                'follower_id' => $request->user->user_id,
                                'event_title' => $request->title    
                            ];
                    }
                }
                if(count($groupUserData)>0) {
                    event(new NotificationEvent($groupUserData,Notification::EVENT_CREATE));
                }

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.event_added'), $event_data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to edit event
     * @param $request 
     * @return response json
     */
    public function EditEvent(EventRequest $request) {
        try {
            $request->user_id = $request->user->user_id;
            $data = $this->event->insert($request, $request->eventId);

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.event_update'), $data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to delete event
     * @param $request 
     * @return response json
     */
    public function deleteEvent(\App\Http\Requests\v1\DeleteEventRequest $request) {
        try {
            $event = $this->event->get($request->eventId);
            if(!empty($event->file_name)) {
                $path = public_path().Event::EVENT_FULL_IMG;
                $thumb = public_path().Event::EVENT_THUMB_IMG;    
                @unlink($path.$event->file_name);
                @unlink($thumb.$event->file_name);
            }
            $this->event->delete($request->eventId);

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.event_delete'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to event list
     * @param $request 
     * @return response json
     */
    public function eventList(\App\Http\Requests\v1\EventListRequest $request) {
        try {
           
            $data = $this->event->all($request,$request->user->user_id);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'),$data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }

    /**
     * function is used to save event image
     * @param $request 
     * @return response json
     */
    public static function saveImage($id, $image) {
        $img = Utility::saveImage($image, public_path() . Event::EVENT_FULL_IMG, public_path() . Event::EVENT_THUMB_IMG);
        $img_path = url(Event::EVENT_FULL_IMG.$img);
        $thumb_path = url(Event::EVENT_THUMB_IMG.$img);
        Event::updateImage($id, ['image' => $img_path,'thumb' => $thumb_path,'file_name' => $img]);
    }

}
