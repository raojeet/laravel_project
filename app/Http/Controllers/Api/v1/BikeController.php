<?php

/**
 * Bike controller
 *
 * This controller handles authenticating users for the application
 * and provide user details for use it also register new users via email
 *
 * @class      UserController
 * @author     Jitender <jeet.yadav25@gmail.com>
 * @version    Release: v1
 */

namespace App\Http\Controllers\Api\v1;

use App\Utility\Utility;
use App\Http\Requests\v1\BikeRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Bike;
use App\Http\Requests\v1\BikeImageRequest;
use Illuminate\Support\Facades\DB;

Class BikeController extends \App\Http\Controllers\Controller {
    /*
      |--------------------------------------------------------------------------
      | Bike Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | provide post details for use it also register new users via email.
      |
     */

    public function __construct() {
        $this->utility = new Utility();
    }

    /**
     * function is used to add bike
     * @param $request 
     * @return response json
     */
    public function addBike(BikeRequest $request) {
        try {
            
            Bike::saveUpdateBike($request);

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.bike_added'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to add bikes images
     * @param $request 
     * @return response json
     */
    public function saveImage(BikeImageRequest $request) {
        try {
            ini_set('memory_limit', '-1');
            foreach($request->file('bikeImage') as $image) {
                $img = Utility::saveImage($image, public_path().Bike::BIKE_FULL_IMG, public_path().Bike::BIKE_THUMB_IMG);
                $inputs[] = [
                    'file_name' => $img,
                    'thumb_url' => url(Bike::BIKE_THUMB_IMG.$img),
                    'full_url' => url(Bike::BIKE_FULL_IMG.$img),
                    'bike_id' => $request->bikeId,
                ];
            }
            DB::table('bike_images')->insert($inputs);

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.bike_image_added'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
}
