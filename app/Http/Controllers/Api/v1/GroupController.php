<?php

/**
 * Group controller
 *
 * This controller handles authenticating users for the application
 * and provide user details for use it also register new users via email
 *
 * @class      GroupController
 * @author     jeet <author@example.com>
 * @version    Release: v1
 */

namespace App\Http\Controllers\Api\v1;

use App\Utility\Utility;
use App\Http\Requests\v1\GroupRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Group;
use App\Http\Requests\v1\GroupListRequest;
use App\Http\Requests\v1\GroupDetailsRequest;
use App\Http\Requests\v1\MyGroupListRequest;
use App\Events\NotificationEvent;
use App\Models\Notification;
use App\Models\GroupUser;
use App\Models\User;

Class GroupController extends \App\Http\Controllers\Controller {
    /*
      |--------------------------------------------------------------------------
      | Group Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | provide post details for use it also register new users via email.
      |
     */

    public function __construct() {
        $this->utility = new Utility();
    }

    /**
     * function is used to add group
     * @param $request 
     * @return response json
     */
    public function addGroup(GroupRequest $request) {
        try {
            if(empty($request->user->is_verify) || empty($request->user->is_email_verify)) {
                return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.email_verify'));
            }
            $request->user_id = $request->user->user_id;
            $data = Group::store($request);
            $result['group'] = $data->toArray();
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.post_added'), $result);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }

    /**
     * function is used to save image
     * @param $request 
     * @return response json
     */
    public function saveImage(\App\Http\Requests\v1\GroupImageRequest $request) {
        try {
            $image = Utility::saveImage($request->file('image'), public_path() . Group::GROUP_FULL_IMG, public_path() . Group::GROUP_THUMB_IMG);
            $img_path = url(Group::GROUP_THUMB_IMG . $image);
            Group::updateImage($request->groupId,['image' => $img_path,'file_name' => $image]);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to follow Group
     * @param $request 
     * @return response json
     */
    public function followGroup(\App\Http\Requests\v1\GroupFollowRequest $request) {
        try {
            $count = \App\Models\GroupFollow::whereGroupId($request->groupId)->
                    whereUserId($request->user->user_id)->count();
            
            if($count > 0 && ($request->status == \App\Models\GroupFollow::FOLLOW)) {
               return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.already_follow'));  
            }
            \App\Models\GroupFollow::follow($request);
            // code to send notification
           $data = array();
           $groupAdminData = Group::find($request->groupId);
           if(!is_null($groupAdminData)){
               $data[] = [ 'following_id' => $groupAdminData->user_id,
                   'follower_id' => $request->user->user_id, 
                   'follower_name' => $request->user->name, 
                   'group_name' => $groupAdminData->title, 
                   'group_id' => $groupAdminData->id     
                ];
            }
            if(count($data)>0) {
                event(new NotificationEvent($data,Notification::GROUP_FOLLOW));
            }
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.group_follow_success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to join Group
     * @param $request 
     * @return response json
     */
    public function joinGroup(\App\Http\Requests\v1\GroupUserRequest $request) {
        try {
            \App\Models\GroupUser::join($request);

            $data = array();
            $groupAdminData = Group::find($request->groupId);
            if(!is_null($groupAdminData)){
               $data[] = [ 'following_id' => $groupAdminData->user_id,
                   'follower_id' => $request->user->user_id,  
                   'follower_name' => $request->user->name, 
                   'group_name' => $groupAdminData->title, 
                   'group_id' => $groupAdminData->id     
                ];
            }
            if(count($data)>0) {
                event(new NotificationEvent($data,Notification::GROUP_JOIN));
            }

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    /**
     * function is used to follow Group
     * @param $request 
     * @return response json
     */
    public function acceptRequestGroup(\App\Http\Requests\v1\GroupUserRequest $request) {
        try {
            \App\Models\GroupUser::acceptRequestGroup($request);

            // Update Current Notification staus
            $notification = Notification::where('id',$request->notificationId)->first();
            $notification->group_status = \App\Models\GroupUser::ACCEPTED;
            $notification->save();

            $data = array();
            $groupAdminData = Group::find($request->groupId);
            $userData = User::where('id',$request->senderUserId)->first();
            if(!is_null($groupAdminData)){
               $data[] = [ 'following_id' => $request->senderUserId,
                   'follower_id' => $groupAdminData->user_id,  
                   'group_name' => $groupAdminData->title,
                   'follower_name' => $userData->name
                ];
            }
            if(count($data)>0) {
                event(new NotificationEvent($data,Notification::GROUP_JOIN_ACCEPT));
            }

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    /**
     * function is used to follow Group
     * @param $request 
     * @return response json
     */
    public function rejectRequestGroup(\App\Http\Requests\v1\GroupUserRequest $request) {
        try {
            // Update Current Notification status
            $notification = Notification::where('id',$request->notificationId)->first();
            $notification->group_status = \App\Models\GroupUser::REJECTED;;
            $notification->save();

            \App\Models\GroupUser::rejectRequestGroup($request);

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    /**
     * function is used to Group List
     * @param $request 
     * @return response json
     */
    public function groupList(GroupListRequest $request) {
        try {
            $data = Group::getGroupList($request,$request->user->user_id);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'),$data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to Group Details
     * @param $request 
     * @return response json
     */
    public function groupDetails(GroupDetailsRequest $request) {
        try {
            $data['groupDetails'] = Group::getGroupDetails($request);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'),$data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }


    /**
     * function is used to Group List
     * @param $request 
     * @return response json
     */
    public function getMyGroupList(MyGroupListRequest $request) {
        try {
            $data['myGroupList'] = Group::getMyGroupList($request->user->user_id);
            return $this->utility->renderJson(Response::HTTP_OK, trans('api.success'),$data);
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
    
    /**
     * function is used to delete user from group
     * @param $request 
     * @return response json
     */
    public function deleteUser(\App\Http\Requests\v1\DeleteGroupUserRequest $request) {
        try { 
            \App\Models\GroupUser::whereUserId($request->userId)->delete();

            return $this->utility->renderJson(Response::HTTP_OK, trans('api.user_delete'));
        } catch (\Exception $e) {
            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
            return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.error'));
        }
    }
}
