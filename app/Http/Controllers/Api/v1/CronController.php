<?php

/**
 * Cron Controller
 *
 * This controller handles cron jobs for the application
 *
 * @class      CronController
 * @author     Appster <author@example.com>
 * @version    Release: v1
 */

namespace App\Http\Controllers\Api\v1;

use App\Utility\Utility;
use App\Models\Notification;

Class CronController extends \App\Http\Controllers\Controller {
    /*
      |--------------------------------------------------------------------------
      | Cron Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles cron jobs and
      | send related notifications to associated users The controller uses a
      | PushNotification library to send push notification.
      |
     */

    public function __construct() {
        $this->utility = new Utility();
    }

    /**
     * function is used to sent event notification.
     * @param  $request
     * @return void
     */
    public function pushNotification() {
        //try {
            $push_data = Notification::getList();
            //dd($push_data);
            if (count($push_data)>0) {
                Utility::sendNotification($push_data);
            }
            return 'sent';
//        } catch (\Exception $e) {
//            Utility::logException(__METHOD__, $e->getFile(), $e->getLine(), $e->getMessage());
//            return false;
//        }
    }

}
