<?php

namespace App\Http\Controllers;

use App\Services\PannelAdmin;

class HomeController extends Controller {
    
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles all dashboard related functionalities. 
      | In this controller we fetch information regarding number of users, users and questions.
      |
     */
    
    /**
     * Create a new HomeController instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    /**
     * Show the admin panel.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke() {
        $pannels = [];
        $configPannels = config('admin.pannels');
        foreach ($configPannels as $pannel) {
            array_push($pannels, new PannelAdmin($pannel));
        }

        return view('dashboard.home', compact('pannels'));
    }

}
