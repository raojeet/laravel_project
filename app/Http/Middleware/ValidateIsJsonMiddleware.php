<?php

namespace App\Http\Middleware;

use Closure;
use App\Utility\Utility;
use Symfony\Component\HttpFoundation\Response;

class ValidateIsJsonMiddleware {

    public function __construct() {

        $this->utility = new Utility();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (($request->isMethod('POST') || $request->isMethod('PUT')) && !empty($request->getContent())) {
            
            json_decode($request->getContent());
            if (json_last_error() != JSON_ERROR_NONE) {
                return $this->utility->renderJson(Response::HTTP_BAD_REQUEST, trans('api.invalid_json'));
            }
        }
        // code to Sanitize user  
        $input = $request->all();
        array_walk_recursive($input, function(&$input) {
            $input = strip_tags(trim($input));
        });

        $request->merge($input);
        return $next($request);
    }

}
