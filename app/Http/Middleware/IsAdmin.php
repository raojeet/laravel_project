<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (session('status') === 'admin' || isset(\Auth::User()->role)) {
            return $next($request);
        }
        return redirect('/');
    }

}
