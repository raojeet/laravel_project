<?php

namespace App\Http\Middleware;

use Closure;
use App\Utility\Utility;
use Symfony\Component\HttpFoundation\Response;

class ApiAuthentication {

    //Constants
    const SESSION_EXPIRE = 101;

    public function __construct() {

        $this->common = new Utility();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $sessionId = $request->header('sessionId');
        if (!empty($sessionId)) {
            $result = \App\Models\UserSession::getUserBySessionId($sessionId);
            if (is_null($result)) {
                return $this->common->renderJson(Response::HTTP_UNAUTHORIZED, trans('api.session_expire'));
            }
        } if ($sessionId == '' || empty($sessionId)) {
            return $this->common->renderJson(Response::HTTP_BAD_REQUEST, trans('api.session_require'));
        }
        $request->merge(["user" => $result]);

        return $next($request);
    }

}
