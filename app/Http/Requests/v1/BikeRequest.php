<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class BikeRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'bikeCompany' => 'required',
            'bikeModal' => 'required',
            'bikeNumber' => 'required',
            'registrationExpiryDate' => 'required|date_format:Y-m-d'
        ];
    }
    

}
