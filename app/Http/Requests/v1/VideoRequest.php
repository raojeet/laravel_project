<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class VideoRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'video' => 'required|mimes:mp4,ogx,oga,ogv,ogg,webm|max:50000000',
            'postId' => 'sometimes|nullable|exists:'.(new \App\Models\Post())->getTable().',id',
        ];
    }

}
