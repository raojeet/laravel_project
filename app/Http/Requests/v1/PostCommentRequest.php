<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class PostCommentRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'comment' => 'required|string',
            'postId' => 'required|nullable|exists:'.(new \App\Models\Post())->getTable().',id',
        ];
    }
    

}
