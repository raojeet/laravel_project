<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class ImageRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'image' => 'required|mimes:png,jpeg,jpg,gif|max:5000000',
            'postId' => 'sometimes|nullable|exists:'.(new \App\Models\Post())->getTable().',id',
            'postType' => 'integer|in:1,2',
            'eventId' => 'sometimes|nullable|exists:'.(new \App\Models\Event())->getTable().',id',
        ];
    }

}
