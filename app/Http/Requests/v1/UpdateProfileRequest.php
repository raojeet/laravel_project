<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class UpdateProfileRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'dob' => 'required|date_format:Y-m-d',
            'gender' => 'required|in:M,F',
            'city' =>'string|max:250',
            'email' => 'required|unique:'.(new \App\Models\User())->getTable().',email,'.$this->request->get('user')->user_id.',id',
        ];
    }

}
