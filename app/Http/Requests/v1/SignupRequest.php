<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class SignupRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required|regex:/^[\pL\s\-]+$/u',
            'password' => 'required|string',
            'phone' => 'required|regex:/^[789]\d{9}$/|exists:'.(new \App\Models\User())->getTable().',phone,is_verify,1',
            'userName' => 'required|unique:'.(new \App\Models\User())->getTable().',username',
            'email' => 'sometimes|email|unique:'.(new \App\Models\User())->getTable().',email',
            'age' => 'integer',
            'gender' => 'required',
            'email' => 'email',
            'city' =>'string|max:250'
        ];
    }
    
     /**
     * Custom validation rule.
     *
     * @return array
     */
    public function messages() {
        return [
            'phone.regex' => trans('api.invalid_phone'),
            'phone.exists' => trans('api.not_verified')
        ];
    }

}
