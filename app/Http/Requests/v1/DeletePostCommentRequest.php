<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class DeletePostCommentRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'postCommentId' => 'required|exists:'.(new \App\Models\PostComment())->getTable().',id',
            'postId' => 'required|exists:'.(new \App\Models\Post())->getTable().',id',
        ];
    }

}
