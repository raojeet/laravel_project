<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;
use App\Models\GroupUser;

class GroupUserRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'notificationId'=> 'sometimes|nullable|exists:'.(new \App\Models\Notification())->getTable().',id',
            'groupId' => 'required|integer|exists:'.(new \App\Models\Group())->getTable().',id',
            'senderUserId' => 'sometimes|nullable|exists:'.(new \App\Models\User())->getTable().',id',
            'status' => 'required|in:1,2,3',
        ];
    }

}
