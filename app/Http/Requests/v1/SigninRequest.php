<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

use App\Models\UserSession;

class SigninRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'phone' => 'required|regex:/^[789]\d{9}$/|exists:'.(new \App\Models\User())->getTable().',phone,is_verify,1',
            'password' => 'required|string',
            'deviceType' => 'required|in:' . UserSession::IPHONE . ',' . UserSession::ANDROID,
            'deviceToken' => 'sometimes|nullable|string'
        ];
    }
    
    /**
     * Custom validation rule.
     *
     * @return array
     */
    public function messages() {
        return [
            'phone.regex' => trans('api.invalid_phone'),
            'phone.exists' => trans('api.not_verified')
        ];
    }

}
