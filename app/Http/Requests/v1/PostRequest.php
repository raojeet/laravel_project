<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class PostRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'postDescription' => 'required|string',
            'location' => 'string',
            'postType' => 'required|integer|in:0,1,2',
            'postUrl' => 'string',
            'postId' => 'sometimes|nullable|exists:'.(new \App\Models\Post())->getTable().',id',
            'groupId' => 'sometimes|integer|exists:'.(new \App\Models\Group())->getTable().',id',
        ];
    }
    

}
