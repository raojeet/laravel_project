<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class VerifyOtpRequest extends BaseRequest {
    
    public function __construct(ValidationFactory $validationFactory) {

        $validationFactory->extend(
                'checkOtp', function ($attribute, $value, $parameters, $validator) {
            $user = \App\Models\User::whereVerificationOtp($value)
                    ->wherePhone($validator->getData()['phone'])->first();
            return (!isset($user->id)) ? false : true;
        }, trans('api.invalid_otp')
        );
    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'phone' => 'required|regex:/^[789]\d{9}$/|exists:'.(new \App\Models\User())->getTable().',phone',
            'otp' => 'required|checkOtp:'.(new \App\Models\User())->getTable().',verification_otps,phone,phone'
        ];
    }
    
     /**
     * Custom validation rule.
     *
     * @return array
     */
    public function messages() {
        return [
            'phone.regex' => trans('api.invalid_phone'),
        ];
    }

}
