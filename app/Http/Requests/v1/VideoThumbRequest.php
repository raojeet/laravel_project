<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;
use App\Models\Post;
use App\Models\PostImage;

class VideoThumbRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'videoThumb' => 'required|mimes:png,jpeg,jpg,gif|max:5000000',
            'postId' => 'sometimes|nullable|exists:'.(new \App\Models\Post())->getTable().',id',
            'refrenceId' => 'sometimes|nullable|exists:'.(new \App\Models\PostImage())->getTable().',id',
        ];
    }

}
