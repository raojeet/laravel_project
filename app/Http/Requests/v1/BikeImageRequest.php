<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class BikeImageRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'bikeImage' => 'required|array',
            'bikeImage.*' => 'image|mimes:jpg,jpeg,png|max:5120',
            'bikeId' => 'required|exists:bikes,id,user_id,'.$this->request->get('user')->user_id
        ];
    }
    

}
