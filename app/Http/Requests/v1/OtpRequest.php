<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class OtpRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'phone' => 'required|regex:/^[789]\d{9}$/'
        ];
    }
    
     /**
     * Custom validation rule.
     *
     * @return array
     */
    public function messages() {
        return [
            'phone.regex' => trans('api.invalid_phone')
        ];
    }

}
