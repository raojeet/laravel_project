<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class GroupRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title' => 'required|string|unique:'.(new \App\Models\Group())->getTable().',title',
            'tagLine' => 'required|string',
            'groupDescription' => 'required|string',
            'groupId' => 'sometimes|nullable|exists:'.(new \App\Models\Group())->getTable().',id',
            'city' => 'required|string',
            'state' => 'required|string',
        ];
    }

}
