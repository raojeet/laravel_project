<?php

namespace App\Http\Requests\v1;
use App\Http\Requests\BaseRequest;

class DeleteGroupUserRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'groupId' => 'required|exists:groups,id,user_id,'.$this->request->get('user')->user_id,
            'userId' => 'required|exists:group_users,user_id',
        ];
    }

}
