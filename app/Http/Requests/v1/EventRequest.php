<?php

namespace App\Http\Requests\v1;
use \App\Http\Requests\BaseRequest;

class EventRequest extends BaseRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'title' => 'required',
            'eventDescription' => 'required',
            'eventDate' => 'required|date_format:Y-m-d',
            'eventTime' => 'required|date_format:H:i',
            'location' => 'required',
            'eventType' => 'required|in:0,1',
            'latitude' => 'required',
            'longitude' => 'required',
            'eventId' => 'sometimes|nullable|exists:'.(new \App\Models\Event())->getTable().',id',
        ];
    }

}
