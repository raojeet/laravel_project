<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification implements ShouldQueue {

    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token) {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable) {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail() {
        return (new MailMessage)
                        ->from(env('MAIL_FROM'), env('APP_NAME'))
                        ->subject(trans('admin/password.forgot_email_subject'))
                        ->line([
                            trans('admin/password.email-intro'),
                            trans('admin/password.email-click'),
                        ])
                        ->action(trans('admin/password.email-button'), url('password/reset', $this->token))
                        ->line(trans('admin/password.email-end'));
    }

}
