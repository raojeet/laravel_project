<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Mailing extends Mailable {

    use Queueable,
        SerializesModels;
    
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        
        return $this->from(env('MAIL_FROM'), config('constants.APP_NAME'))
            ->subject($this->data['subject'])
            ->view('emails.'.$this->data['view'])
            ->with(['data' => $this->data]);
    }

}
