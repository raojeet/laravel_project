<?php

namespace App\Utility;

use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Intervention\Image\Facades\Image;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use App\Models\Notification;
use stdClass;

class Utility {
    
    const LIMIT = 6;
    
    /**
     * Json response outout
     *
     * return result in json format
     * @param status string(), status code integer(), message string(), data array()
     * @return json
     */
    public function renderJson($response_code, $message = false, $data = array()) {
        
        if ((isset($data['groupDetails']) && count($data['groupDetails'])) > 0 
                || (isset($data['posts']) && count($data['posts']) > 0) 
                || (isset($data['upcoming']) && count($data['upcoming']) > 0) 
                || (isset($data['all']) && count($data['all']) > 0)
                || (isset($data['invited']) && count($data['invited']) > 0)
                || (isset($data['groupPopular']) && count($data['groupPopular']) > 0)
                || (isset($data['groupFollowing']) && count($data['groupFollowing']) > 0)
                || (isset($data['groupMy']) && count($data['groupMy']) > 0)
                || (isset($data['allNotifications']) && count($data['allNotifications']) > 0)
                || (isset($data['invites']) && count($data['invites']) > 0)
            ) 
        {
            $this->response['result'] = $this->convertToCamelCase($data, true);
        } else {
            $this->response['result'] = $this->convertToCamelCase($data);
        }
        $this->response['message'] = $message;
        
        return Response()->json($this->response,$response_code)->header('Content-Type', "application/json");
    }

    /**
     * Generate random string 
     *
     * Generate random string
     * @param limit integer
     * @return string
     */
    public static function logException($method, $file, $line, $message) {
        Log::error(['method' => $method, 'error' => ['file' => $file, 'line' => $line, 'message' => $message], 'created_at' => date("Y-m-d H:i:s")]);
    }
    
    /**
     * Generate session token
     *
     * Generate unique md5 hashed string
     * @param null
     * @return string
     */
    public static function generateToken() {
        $time = time();
        $str = 'abcdefghijklmnopqrstuvwxyz0123456789' . "$time";
        $shuffled = str_shuffle($str);

        return md5($shuffled);
    }

    /**
     * function is used to save image file to s3 storage
     * 
     * @param multipart file
     * @return file name(string)
     */
    public static function saveImage($image,$path,$thumb_path) {
        $extension = $image->getClientOriginalExtension();
        if($extension != 'jpg' && $extension != 'png' && $extension != 'jpeg') {
            $extension = 'jpg';
        }
        $filename = time().'.'.$extension;
        $img = Image::make($image);
        if(!empty($thumb_path)){
            $img->save($path.$filename)
                ->resize(Config::get('constants.thumb_width'), null)
                ->save($thumb_path.$filename);
        } else {
            $img->save($path.$filename);
        }
        return $filename;
    }

    /**
     * function is used to save video file to s3 storage
     * 
     * @param multipart file
     * @return file name(string)
     */
    public static function saveVideo($video,$path,$file) {
        $extension = $video->getClientOriginalExtension();
        $filename = time().'.'.$extension;
        $file->move($path, $filename);
        return $filename;
    }

    /**
     * function is used to save video file to s3 storage
     * 
     * @param multipart file
     * @return file name(string)
     */
    public static function saveVideoThumb($video,$path,$file) {
        $extension = $video->getClientOriginalExtension();
        $filename = time().'.'.$extension;
        $file->move($path, $filename);
        return $filename;
    }
    
    /**
     * Generate random string 
     *
     * Generate random string
     * @param limit integer
     * @return string
     */
    public static function randomString($limit = self::LIMIT) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        for ($i = 0; $i < $limit; $i++) {
            $n = rand(0, strlen($alphabet) - 1);
            $pass[$i] = $alphabet[$n];
        }
        return implode($pass);
    }

    /**
     * Convert to Camel Case
     *
     * Converts array keys to camelCase, recursively.
     * @param  array  $array Original array
     * @return array
     */
    protected function convertToCamelCase($array, $flag = false) {
        $converted_array = [];
        foreach ($array as $old_key => $value) {
            if (is_array($value)) {
                $value = $this->convertToCamelCase($value, $flag);
            } else if (is_object($value)) {
                if ($value instanceof Model || method_exists($value, 'toArray')) {
                    $value = $value->toArray();
                } else {
                    $value = (array) $value;
                }
                $value = $this->convertToCamelCase($value, $flag);
            }
            $converted_array[camel_case($old_key)] = $value;
        }

        if ($flag) {
            $result = $converted_array;
        } else {
            $result = (empty($converted_array)) ? new stdClass() : $converted_array;
        }
        return $result;
    }
    
    /**
     * get age from dob
     *
     * Converts array keys to camelCase, recursively.
     * @param  array  $date Original array
     * @return integer
     */
    public function getAge($date) {
        $dob = new Carbon($date);
        $now = Carbon::now();
        return $dob->diffInYears($now);
    }

    /**
     * @@Version 1 code please do not change 
     * function is used to send push notification to ios users
     * @param array $data
     * @return boolean
     */
    public static function sendNotification($data) {
        $ids = [];
        $in_process = array_pluck($data, 'id');
       // Notification::whereIn('id', $in_process)->update(['is_sent' => Notification::PROCESS]);
        foreach ($data as $push) {
            $ids[] = $push->id;
            
            $notification = PushNotification::app('androidApp');
            $token = $push->device_token;
            $device = PushNotification::Device($token);
            $message = PushNotification::Message($push->title, [
                        'custom' => [
                            'badge' => $push->badge,
                            'sound' => 'bell.aiff',
                            'launchImage' => 'notification.jpg',
                            'type' => 1,
                            'notificationId' => $push->id
                        ]]
            );
            $notification->to($device)->send($message);
            dd($notification);
        }
        Notification::whereIn('id', $ids)->update(['is_sent' => Notification::SENT]);
        return true;
    }
    
}
