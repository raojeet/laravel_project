<?php

namespace App\Listeners;

use App\Events\NotificationEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Notification;
use Illuminate\Support\Facades\DB;
use App\Models\GroupUser;

class SendNotification implements ShouldQueue {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        
    }

    /**
     * Handle the event.
     *
     * @param  NotificationEvent  $event
     * @return void
     */
    public function handle(NotificationEvent $event) {
        $action = $event->action;
        $i = 0;
        
        foreach ($event->notification as $value) {
            switch ($action) {
                case Notification::USER_FOLLOW:
                    $insert_data[] = [
                        'sender_user_id' =>  $value['follower_id'],
                        'reciever_user_id' => $value['following_id'],
                    ];
                    $insert_data[$i]['title'] = trans('notification.user_follow_title');
                    $insert_data[$i]['message'] = $value['follower_name']." has started following you.";
                    break;
                case Notification::GROUP_FOLLOW:
                    $insert_data[] = [
                        'sender_user_id' =>  $value['follower_id'],
                        'reciever_user_id' => $value['following_id'],
                    ];
                    $insert_data[$i]['title'] = trans('notification.group_follow_title');
                    $insert_data[$i]['message'] = $value['follower_name']." has started following the group ".$value['group_name'].".";
                    $insert_data[$i]['group_id'] = $value['group_id'];
                    break;
                case Notification::GROUP_JOIN:
                    $insert_data[] = [
                        'sender_user_id' =>  $value['follower_id'],
                        'reciever_user_id' => $value['following_id'],
                    ];
                    $insert_data[$i]['title'] = trans('notification.group_join_title');
                    $insert_data[$i]['message'] = $value['follower_name']." has requested to join the group ".$value['group_name'].".";
                    $insert_data[$i]['group_id'] = $value['group_id'];
                    $insert_data[$i]['group_status'] = GroupUser::PENDING;
                    break;
                case Notification::EVENT_CREATE:
                    $insert_data[] = [
                        'sender_user_id' =>  $value['follower_id'],
                        'reciever_user_id' => $value['following_id'],
                    ];
                    $insert_data[$i]['title'] = trans('notification.group_event_title');
                    $insert_data[$i]['message'] = $value['event_title']." events arround you.";
                    break;
                case Notification::GROUP_JOIN_ACCEPT:
                    $insert_data[] = [
                        'sender_user_id' =>  $value['follower_id'],
                        'reciever_user_id' => $value['following_id'],
                    ];
                    $insert_data[$i]['title'] = trans('notification.group_join_title');
                    $insert_data[$i]['message'] = $value['follower_name']." you're now a member of the group  ".$value['group_name'].".See what other memebers are doing there.";

                    break;
                case Notification::POST_LIKE:
                    $insert_data[] = [
                        'sender_user_id' =>  $value['follower_id'],
                        'reciever_user_id' => $value['following_id'],
                    ];
                    $insert_data[$i]['title'] = trans('notification.post_like_title');
                    $insert_data[$i]['message'] = $value['follower_name']." has like your post.";
                    break;
                case Notification::POST_COMMENT:
                    $insert_data[] = [
                        'sender_user_id' =>  $value['follower_id'],
                        'reciever_user_id' => $value['following_id'],
                    ];
                    $insert_data[$i]['title'] = trans('notification.post_comment_title');
                    $insert_data[$i]['message'] = $value['follower_name']." has commented on your post.";
                    break;

                case Notification::POST_CREATE:
                    $insert_data[] = [
                        'sender_user_id' =>  $value['follower_id'],
                        'reciever_user_id' => $value['following_id'],
                    ];
                    $insert_data[$i]['title'] = trans('notification.post_create_title');
                    $insert_data[$i]['message'] = $value['follower_name']." has shared a post.";
                    break;
                case Notification::GROUP_POST_CREATE:
                    $insert_data[] = [
                        'sender_user_id' =>  $value['follower_id'],
                        'reciever_user_id' => $value['following_id'],
                    ];
                    $insert_data[$i]['title'] = trans('notification.post_create_title');
                    $insert_data[$i]['message'] = $value['follower_name']." has shared a post.";
                    break;
                default:
                    break;
            }
            $i++;
        }
        
        DB::table('notifications')->insert($insert_data);
    }

}
