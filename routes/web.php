<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

use Illuminate\Http\Request;

Route::get('/', 'Auth\LoginController@showLoginForm');

// Authentication 
Auth::routes();

Route::group(['middleware' => ['admin']], function () {
    Route::get('home', 'HomeController')->name('home');
    Route::get('password/change', function () {
        return view('auth/passwords.password');
    });
    Route::get('bikers/getUserDetails/{id}', 'UserController@getUserDetails');
    Route::post('password/change', 'UserController@changePassword');
    Route::post('group/approve/{id}', 'GroupController@approve');

    Route::get('groups', function () {
        return view('groups/list');
    });
    Route::get('/datatables/groupData', ['uses' => 'GroupController@groupList'])->name('datatables.allGroupData');

    Route::get('users', function () {
        return view('users/list');
    });
    Route::get('/datatables/userData', ['uses' => 'UserController@userList'])->name('datatables.allUserData');

    Route::get('bikers', function () {
        return view('bikers/list');
    });
    Route::get('/datatables/bikerData', ['uses' => 'UserController@bikerList'])->name('datatables.allBikerData');
});

Route::get('cron', 'Api\v1\CronController@pushNotification');