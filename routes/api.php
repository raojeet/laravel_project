<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('v1/user/verifyEmail/{code}', 'Api\v1\UserController@verifyEmail');
Route::group(['prefix' => 'v1'], function() {
    Route::group(['middleware' => ['isjson']], function() {
        Route::post('sendOtp', ['uses' => 'Api\v1\UserController@sendOtp']);
        Route::post('verifyOtp', ['uses' => 'Api\v1\UserController@verifyOtp']);
        Route::post('signup', ['uses' => 'Api\v1\UserController@signup']);
        Route::post('signin', ['uses' => 'Api\v1\UserController@signin']);
        Route::post('forgotPassword', ['uses' => 'Api\v1\UserController@resetPassword']);
        
        ## authentication required to access below routes
        Route::group(['middleware' => ['apiAuth']], function() {
            Route::post('logout', ['uses' => 'Api\v1\UserController@logout']);
            Route::post('updateProfile', ['uses' => 'Api\v1\UserController@updateProfile']);
            Route::post('getProfile', ['uses' => 'Api\v1\UserController@getProfile']);
            
            Route::post('addEvent', ['uses' => 'Api\v1\EventController@addEvent']);
            Route::post('editEvent', ['uses' => 'Api\v1\EventController@editEvent']);
            Route::post('deleteEvent', ['uses' => 'Api\v1\EventController@deleteEvent']);
            Route::post('events', ['uses' => 'Api\v1\EventController@eventList']);

            Route::post('addPost', ['uses' => 'Api\v1\PostController@addPost']);
            Route::post('editPost', ['uses' => 'Api\v1\PostController@editPost']);
            Route::post('deletePost', ['uses' => 'Api\v1\PostController@deletePost']);
            Route::post('posts', ['uses' => 'Api\v1\PostController@postList']);
            Route::post('likePost', ['uses' => 'Api\v1\PostController@likePost']);


            Route::post('addPostComment', ['uses' => 'Api\v1\PostCommentController@addPostComment']);
            Route::post('deletePostComment', ['uses' => 'Api\v1\PostCommentController@deletePostComment']);
            Route::post('postCommentList', ['uses' => 'Api\v1\PostCommentController@postCommentList']);
            

            Route::post('updateToken', ['uses' => 'Api\v1\UserController@updateDeviceToken']);
            Route::post('addGroup', ['uses' => 'Api\v1\GroupController@addGroup']);
            Route::post('editGroup', ['uses' => 'Api\v1\GroupController@addGroup']);
            Route::post('contactList', ['uses' => 'Api\v1\UserFollowController@followList']);
            Route::post('followUser', ['uses' => 'Api\v1\UserFollowController@followUser']);
            Route::post('followGroup', ['uses' => 'Api\v1\GroupController@followGroup']);
            Route::post('joinGroup', ['uses' => 'Api\v1\GroupController@joinGroup']);
            Route::post('acceptRequestGroup', ['uses' => 'Api\v1\GroupController@acceptRequestGroup']);
            Route::post('rejectRequestGroup', ['uses' => 'Api\v1\GroupController@rejectRequestGroup']);
            Route::post('groups', ['uses' => 'Api\v1\GroupController@groupList']);
            Route::post('groupDetails', ['uses' => 'Api\v1\GroupController@groupDetails']);
            Route::post('getMyGroupList', ['uses' => 'Api\v1\GroupController@getMyGroupList']);
            Route::post('notifications', ['uses' => 'Api\v1\NotificationController@getNotification']);
            Route::post('addBike', ['uses' => 'Api\v1\BikeController@addBike']);
            Route::post('deleteGroupUser', ['uses' => 'Api\v1\GroupController@deleteUser']);
            
        });
    });
    Route::get('cron', ['uses' => 'Api\v1\CronController@pushNotification']);
    Route::post('saveVideoThumb', ['middleware' => ['apiAuth'], 'uses' => 'Api\v1\PostController@saveVideoThumb']);
    Route::post('saveVideo', ['middleware' => ['apiAuth'], 'uses' => 'Api\v1\PostController@saveVideo']);
    Route::post('saveImage', ['middleware' => ['apiAuth'], 'uses' => 'Api\v1\UserController@saveImage']);
    Route::post('saveGroupImage', ['middleware' => ['apiAuth'], 'uses' => 'Api\v1\GroupController@saveImage']);
    Route::post('bikeImage', ['middleware' => ['apiAuth'], 'uses' => 'Api\v1\BikeController@saveImage']);
});
